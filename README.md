This project was bootstrapped with [Create React App](https://github.com/facebook/create-react-app).
## env.js / config file
This file is used to get enviromental variable to setup at one place. which can be changed as per usage. It has some parameters. Here I have listed params and usage of it.
socketUrl: To change socket url like ws://localhost:3001
channel: Channel media of socket like socket

## color codes

Color codes and fonts are added on variable.scss
Location of variable file is `src/variable.scss`
```js
/* fonts */
$font-700: "museo_sans700";
$font-300: "museo_sans300";
/* main screen */
$color-white: #fefefe;
$scroll-background: rgba(0, 0, 0, .2);
/* Initial Screen */
$color-blue:  #002b43;
$user-profile-bg: #d0c9c9;
$color-green:  #00ff6c;
$color-pure-white: #fff;
$color-light-blue: #00b8ec;
$color-grey: #707070;
/* Chat Screen */
$color-white-f5: #f5f5f5;
$color-grey-lighter: #646464;
$color-blue-lighter: #20cef5;
$color-grey-shade: #4c4c4c;
$color-black-text: #bab7b7;
$color-black-shadow: rgba(0, 0, 0, 0.16);
$color-pure-black: rgba(0, 0, 0, 0);
$color-grey-shadow: #ccc;
```

## Available Scripts

In the project directory, you can run:

### `npm start`

Runs the app in the development mode.<br>
Open [http://localhost:3000](http://localhost:3000) to view it in the browser.

The page will reload if you make edits.<br>
You will also see any lint errors in the console.

### `npm test`

Launches the test runner in the interactive watch mode.<br>
See the section about [running tests](https://facebook.github.io/create-react-app/docs/running-tests) for more information.

### `npm run build`

##### Your home page url attribute of `homepage` need to add in package.json. This will generate directory based url access like domain.com/directory

Builds the app for production to the `build` folder.<br>
It correctly bundles React in production mode and optimizes the build for the best performance.

The build is minified and the filenames include the hashes.<br>
Your app is ready to be deployed!

######  To deply on server manually just create a zip file of it and extract on directory which we have given package.json file's homepage atrubiute.

See the section about [deployment](https://facebook.github.io/create-react-app/docs/deployment) for more information.


### `npm run eject`

**Note: this is a one-way operation. Once you `eject`, you can’t go back!**

If you aren’t satisfied with the build tool and configuration choices, you can `eject` at any time. This command will remove the single build dependency from your project.

Instead, it will copy all the configuration files and the transitive dependencies (Webpack, Babel, ESLint, etc) right into your project so you have full control over them. All of the commands except `eject` will still work, but they will point to the copied scripts so you can tweak them. At this point you’re on your own.

You don’t have to ever use `eject`. The curated feature set is suitable for small and middle deployments, and you shouldn’t feel obligated to use this feature. However we understand that this tool wouldn’t be useful if you couldn’t customize it when you are ready for it.

## Learn More

You can learn more in the [Create React App documentation](https://facebook.github.io/create-react-app/docs/getting-started).

To learn React, check out the [React documentation](https://reactjs.org/).

