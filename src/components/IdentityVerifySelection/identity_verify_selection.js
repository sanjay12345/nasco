import React from "react";
import "./identity-verify-selection.scss";
import _ from "lodash";

/* load other components */
import DrivingLicenceForm from "./../Forms/DrivingLicenceForm";
import CarForm from "./../Forms/CarForm";

// images
import IcCamera from "./../../public/images/ic_camera.svg";
import IcAttachment from "./../../public/images/ic_attachment.svg";
import IcCompose from "./../../public/images/ic_compose.svg";
import loadingIcon from "./../../public/images/loading.svg";


class IdentityVerifySelection extends React.Component {
	
	constructor(props) {
		super(props);
		this.state = {
			option: "",
			sent: false,
			image: '',
			uploadedImage: '',
			fireCar: false,
			carDetails: {},
			isDialogOpen: false,
			cloudinaryWidget: {},
			startCarFetchDetails: false
		};
		this.selectOption = this.selectOption.bind(this);
		this.sendReply = this.sendReply.bind(this);
		this.onFormSubmit = this.onFormSubmit.bind(this);
		this.onChange = this.onChange.bind(this);
		this.fileUpload = this.fileUpload.bind(this);
		this.closeDialog = this.closeDialog.bind(this);
		this.inputOpenFileRef = React.createRef()
	}

	selectOption(option) {
		if(!_.get(this, "state.sent", false)) {
			this.setState({
				option: option
			});	
			if((option === "UPLOAD") || (option === "CAPTURE")) {
				this.state.cloudinaryWidget.open();
			} else {
				this.setState({
					isDialogOpen: true
				})
			}			
		}		
	}

	openFileDialog = () => {
    this.inputOpenFileRef.current.click()
	}

	closeDialog() {
		this.setState({
			isDialogOpen: false
		});
	}

	sendReply(response) {
		this.setState({
			sent: true,
			isDialogOpen: false
		}, () => {
			this.props.userFormReply(response);
		});

	}

	onFormSubmit(e){
		e.preventDefault() 
		this.fileUpload(this.state.image);
	}

	onChange(e) {
		let files = e.target.files || e.dataTransfer.files;
		if (!files.length)
					return;
		this.createImage(files[0]);		
	}

	createImage(file) {
		let reader = new FileReader();
		reader.onload = (e) => {
			this.setState({
				image: e.target.result
			}, () => {
				this.fileUpload(this.state.image);
			})
		};
		reader.readAsDataURL(file);
	}

	fileUpload(image){
		const url = 'http://thatsbluelogic.com/nascom-api/index.php';
		const formData = new FormData();
		formData.append(0, image);
    
    fetch(url, {
      method: 'POST',
      body: formData
    })
    .then(response => response.json())
		.then(data => {
				this.setState({
					fireCar: true,
					carDetails: data,
					isDialogOpen: true
				})
				}
			)
	}

	componentDidMount() {
		this.setState({
			cloudinaryWidget: window.cloudinary.createUploadWidget({
				cloudName: 'lat-nasco', 
				uploadPreset: 'np66n7ek'}, (error, result) => { 
					this.checkUploadResult(result)
				})
		})		 
	}
	
	checkUploadResult = (resultEvent) => {
		if (resultEvent.event === 'success') {
			if (_.get(this, "props.mode", "") === "car") {
				this.setState({
					image: resultEvent.info.url,
					startCarFetchDetails: true
				})
					this.state.cloudinaryWidget.close({quiet: true});
					let apiurl = "http://thatsbluelogic.com/projects/recognition/car/recognition.php?img_uri="+resultEvent.info.url;
					fetch(apiurl)
						.then(response => response.json())
						.then(data => {
							
						if (_.get(this, "props.mode", "") === "car") {
							this.setState({
								fireCar: true,
								carDetails: data,
								startCarFetchDetails: false,
								isDialogOpen: true
							});
						}
						if (_.get(this, "props.mode", "") === "person") {
							this.setState({
								isDialogOpen: true
							});
						}

						});
			}

			if (_.get(this, "props.mode", "") === "person") {
				this.state.cloudinaryWidget.close({quiet: true});
				this.setState({
					image: resultEvent.info.url,
					isDialogOpen: true
				});
			}
		}
	}

  render() {
		return (
			<React.Fragment>
				<div className="identity-main">
					<div className="identity-button">
						<div className="col-md">
							<span className={"box-button "+ (_.get(this, "state.option", "") === "CAPTURE" ? "active" : "")}
								onClick = {() => this.selectOption("CAPTURE")}
							>
								<div className="selction-icons">
									<span>
										<img src={IcCamera} alt="" />
									</span>
								</div>
								<div className="selction-text">CAPTURE</div>
							</span>
						</div>
						<div className="col-md">
							<span 
								className={"box-button "+ (_.get(this, "state.option", "") === "UPLOAD" ? "active" : "")}
								 onClick = {() => this.selectOption("UPLOAD")}
							>
								<div className="selction-icons">
									<span>
										<img src={IcAttachment} alt="" />
									</span>
								</div>
								<input type="file" ref={this.inputOpenFileRef}  onChange={this.onChange} className="d-none" />
								<div className="selction-text">UPLOAD</div>
							</span>
						</div>
						<div className="col-md">
						<span className={"box-button "+ (_.get(this, "state.option", "") === "MANUAL" ? "active" : "")}
								onClick = {() => this.selectOption("MANUAL")}
							>
								<div className="selction-icons">
									<span>
										<img src={IcCompose} alt="" />
									</span>
								</div>
								<div className="selction-text">MANUAL</div>
							</span>
						</div>
					</div>
					<div className={"identity-photo "+ (_.get(this, "state.image", "") === "" ? "hide" : "")}>
						<img src={ loadingIcon } alt="" className={" loading-icon "+( !_.get(this, "state.startCarFetchDetails", false)  ? "hide" : "")} />
						<img src={ _.get(this, "state.image", "") } alt="" />						
					</div>				
				</div>
				{
					(
						(_.get(this, "props.mode", "") === "person") && 
						(_.get(this, "state.isDialogOpen", false))
					) &&
						<DrivingLicenceForm selectOption={this.selectOption} sendReply={this.sendReply} closeDialog={this.closeDialog}  />
				}
				{
					(
						( (_.get(this, "props.mode", "") === "car") && (_.get(this, "state.isDialogOpen", false)))
					) &&
						<CarForm selectOption={this.selectOption} closeDialog={this.closeDialog} sendReply={this.sendReply} carDetails={_.get(this, "state.carDetails", {})} />
				}
			</React.Fragment>
    );
	}
	
}

export default IdentityVerifySelection;
