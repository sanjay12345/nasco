export const lang_ar = {
    "SUBMIT" : "موافق",
    "MAIN" : "القائمة",
    "MAIN_MENU" : "القائمة الرئيسية",
    "RESTART_SESSION" : "إعادة الجلسة",
    "END": "إنهاء الجلسة",
    "TYPE_A_MESSAGE": "",
    "CONFIRM": "موافق",
    "ENTER_NAME": "اسم",
    "DATE_OF_BIRTH": "تاريخ الميلاد",
    "DATE": "يرجى إدخال التاري",
    "MOBILE_NUMBER": "رقم الهاتف",
    "EMAIL_ID": "البريد الالكتروني",
    "DD": "اليوم",
    "MM": "الشهر",
    "YYYY": "السنه ه",
    "IS_TYPING":"يكتب",
    "RESEND": "إعادة إرسال",
    "SELECT": "اختار"
};