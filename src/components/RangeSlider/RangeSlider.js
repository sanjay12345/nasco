import React from "react";
import Slider from "rc-slider";
import _ from "lodash";
import "rc-slider/assets/index.css";
import "rc-slider/assets/index.css";
import "rc-tooltip/assets/bootstrap.css";
import "./range-slider.scss";

import Tooltip from "rc-tooltip";
const Handle = Slider.Handle;

const handle = props => {
  const { value, dragging, index, ...restProps } = props;
  return (
    <Tooltip
      prefixCls="rc-slider-tooltip"
      overlay={value}
      visible={dragging}
      placement="top"
      key={index}
    >
      <Handle value={value} {...restProps} />
    </Tooltip>
  );
};

class RangeSlider extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      isValueSend: false
    };
  }

  componentDidMount() {}
  componentDidUpdate() {}

  onSliderChange = value => {
    this.setState({
      value
    });
  };
  onAfterChange = value => {
    
  };
  sendRange() {
    if(!(_.get(this, "state.isValueSend", false))) {
			this.setState({
				isValueSend: true
			}, () => {
				this.props.formReply(_.get(this, "state.value", 0)+"")
			});
		}
  }

  renderStatus() {
    if (this.state.value < 3) return <div className="moderate">Mild</div>;
    else if (this.state.value < "7")
      return <div className="moderate">Moderate</div>;
    else if (this.state.value >= "7")
      return <div className="moderate">Severe/Unbearable</div>;
    return <div className="moderate">&nbsp;</div>;
  }
  render() {
    return (      
      <div className="range-slider">
        <Slider
          allowCross={false}
          pushable={true}
          dots
          min={_.get(this, "props.chat.data.min", 0)}
          max={_.get(this, "props.chat.data.max", 10)}
          step={_.get(this, "props.chat.data.step", 1)}
          included={true}
          value={this.state.value}
          onChange={this.onSliderChange}
          onAfterChange={this.onAfterChange}
          disabled={this.props.disabled}
          handle={handle}
          tipProps={{ visible: true }}
        />

        {  this.renderStatus() }
        <div className="submit-box" onClick={ () => this.sendRange()}>
          <i className="fa fa-angle-right" />
        </div>
      </div>
    );
  }
}

export default RangeSlider;
