import React from "react";
import _ from "lodash";
import $ from "jquery";
import "./splash-screen.scss";
import Slider from "react-slick";
import "slick-carousel/slick/slick.css"; 
import "slick-carousel/slick/slick-theme.css";
// images
import SplashLogo from "./../../public/images/splash/logo.svg";
import onboardlogo from "./../../public/images/splash/onboardlogo.svg";
import onboardmobile from "./../../public/images/splash/mobile.svg";
import onboardmobile2 from "./../../public/images/splash/mobile2.svg";
import onboardswitch from "./../../public/images/splash/switch.svg";

class SplashScreen extends React.Component {
  
  constructor(props) {
    super(props);
    this.state = {
      currentSlide: 1
    };
  }

  componentDidMount() {
    $(".splash-logo").fadeTo(5000, 0.0, function() {
      $(this).hide();
    });    
  }

  componentDidUpdate() {}

  render() {
    const settings = {
      arrows: true,
      dots: true,
      infinite: false,
      speed: 1000,
      focusOnSelect: false,
      swipeToSlide: true,
      afterChange: (index) => {
        if(_.get(this, "state.currentSlide", 1) < (index + 1 )) {
          this.setState({
            currentSlide: (index+1)
          }, () => {
            
          });
        }        
      },
      beforeChange: (index) => {
        if (index === 4) {
          this.props.completeonBoarding();
        }
      }
    };

    return (
      <div className={"start-converstion"}>
        <div className="row">
          {/* splash screen start */}

          <div className="splash-main col-12 position-relative hide">
            <div className="splash-logo">
              <img src={SplashLogo} alt="" />
            </div>
          </div>

          {/* splash screen end */}

          {/* onboarding screen start */}

          <div className="oneboarding-screen col-12 position-relative splash-screen-slider">
            <div className="row" />
            <div className="onboard">
              <div className="slider">
              <Slider {...settings}>
                <div className="item">
                  <div className="onboard-logo">
                    <img src={onboardlogo} alt="" width="100px" />
                  </div>
                  <div className="board-content">
                    <div className="text-content">
                      <h1>
                        Pay your Bills,
                        <br />
                        get a Quote &
                        <br />
                        submit your Claims with our Award-Winning Mobile App.
                      </h1>
                    </div>
                  </div>
                </div>

                <div className="item">
                  <div className="onboard-logo">
                    <img src={onboardlogo} alt="" width="100px" />
                  </div>
                  <div className="board-content">
                    <div className="text-content">
                      <h1>Instant Support</h1>
                    </div>
                    <div className="board-mobile">
                      <img src={onboardmobile} alt="" className="" />
                    </div>
                  </div>
                </div>

                <div className="item">
                  <div className="onboard-logo">
                    <img src={onboardlogo} alt="" width="100px" />
                  </div>
                  <div className="board-content">
                    <div className="text-content">
                      <h1>Get Roadside Assistance</h1>
                      <h4 className="mt-3">
                        We'll respond in less
                        <br /> than 5 minutes.{" "}
                      </h4>
                      <h6>
                        It's faster and easier than calling.
                        <br />
                        24/7 assistance.
                        <br />
                        Pre-filled and accessible
                        <br />
                        information after log in.
                      </h6>
                    </div>
                  </div>
                </div>

                <div className="item">
                  <div className="onboard-logo">
                    <img src={onboardlogo} alt="" width="100px" />
                  </div>
                  <div className="board-content">
                    <div className="text-content">
                      <h4>
                        Presence in over 11 countries across the Middle East
                      </h4>
                    </div>
                    <div className="board-mobile">
                      <img src={onboardmobile2} alt="" className="" />
                    </div>
                  </div>
                </div>

                <div className="item">
                  <div className="onboard-logo">
                    <img src={onboardlogo} alt="" width="100px" />
                  </div>
                  <div className="board-content">
                    <div className="text-content">
                      <h1>
                        NASCO Insurance would like to send you notifications
                        about offers and rewards.
                      </h1>
                      <div className="pt-5">
                        <p className="float-left">ALLOW NOTIFICATIONS</p>
                        <span className="float-right board-switch">
                          <img src={onboardswitch} alt="" />
                        </span>
                      </div>
                    </div>
                  </div>
                </div>
                </Slider>
              </div>
            </div>
          </div>
          {/* onboarding screen end */}
        </div>
      </div>
    );
  }
}

export default SplashScreen;
