import React from "react";
import _ from "lodash";
import {ENVIORMENT} from './../../env';
import PoweredBy from "./../PoweredBy/PoweredBy";
import wave from "./../../public/images/wave.png";
import botAvatarmale from "./../../public/images/bot-avatar-Ahmed.png";
import botAvatarfemale from "./../../public/images/bot-avatar-Sara.png";
//images 
import collapsclose from "./../../public/images/collapsclose.svg";

class LayoutTwo extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      selectedAvatar: "",
    };
  }
  
  componentDidMount() {}
  componentDidUpdate() {}

  selectAvatar(avatar) {    
    this.setState({
      selectedAvatar: avatar
    });
  }

  startConversation( lang ) {
    if (_.get(this, "state.selectedAvatar", "") !== "") {
      let dispName = "";
      if( lang === "English") {
        dispName = ( _.get(this, "state.selectedAvatar", "") === "Ahmed" ? "Ahmed" : "Sara");
      } else {
        dispName = ( _.get(this, "state.selectedAvatar", "") === "Ahmed" ? "أحمد" : "سارة");
      }
      this.props.startConversation(
        _.get(this, "state.selectedAvatar", ""),
        dispName,
        lang
      );
    }    
  }
  endConversation() {
    this.props.endConversation();
  }
  render() {
    return (
      <div className={"start-converstion"}>
        <div className="container">
          <div className="row">
            <div className="col position-relative avatar-container">
              <div className="row">
                <div className="cover-pattern">
                <span className="box-header-ic">
                  <span className="btn-link" onClick = {() =>this.endConversation()}>
                    <img src={collapsclose} alt="" />
                  </span>
                </span>


                  <div className="pt-4 pb-4">
                    <p>
                    <h3>
                      Marhaba! <img src={wave} alt="" className="text_ic" /> <span className="font-arabic">مرحبا </span>
                    </h3>
                    <span className="font-arabic font-arabic-18"> انقر على المساعد الإفتراضي الأنسب لك</span>
                    <br />Click on any one of us and we’ll assist you instantly.
                    </p>
                  </div>
                  <div className="user-profile">
                    <div className={"user-profile-male " + ( ((_.get(this, "state.selectedAvatar", "") !== "") && (_.get(this, "state.selectedAvatar", "") !== "Ahmed")) ? "avatar-offline" : "" )}>
                      <span onClick={() => this.selectAvatar("Ahmed")}>
                        <img src={botAvatarmale} alt="" />
                      </span>
                      <span className="status-online"> </span>
                      <h5 className="font-arabic mb-0" onClick={() => this.selectAvatar("Ahmed")}>أحمد</h5>
                      <h5 onClick={() => this.selectAvatar("Ahmed")} className="mt-0">Ahmed</h5>                      
                    </div>
                    <div className={"user-profile-female "  + (((_.get(this, "state.selectedAvatar", "") !== "") && (_.get(this, "state.selectedAvatar", "") !== "Sara")) ? "avatar-offline" : "" )}>
                      <span onClick={() => this.selectAvatar("Sara")}>
                        <img src={botAvatarfemale} alt="" />
                      </span>
                      <span className="status-online"> </span>
                      <h5 onClick={() => this.selectAvatar("Sara")} className="font-arabic mb-0">سارة</h5>
                      <h5 onClick={() => this.selectAvatar("Sara")}  className="mt-0">Sara</h5>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          <div className="text-container">
          <div className="white-pattern">
              <div className="body-text pb-4 pt-5 px-3">
                <p>
                  <span className="font-arabic font-arabic-16">في حالات الطوارئ الرجاء الاتصال على 998</span>
                  <br/>
                  Please call 998 for a Medical Emergency.               
                </p>                
              </div>
              
              <div className="col body-text pt-2 pb-0">
                <button
                  className={"btn col-6 btn-text-white" + ( (_.get(this, "state.selectedAvatar", "") !== "") ? " btn-start": "")}
                  onClick={() => this.startConversation("English")}
                >
                  Let’s Talk
                </button>

                <button
                  className={"btn col-6 btn-text-white font-arabic" + ( (_.get(this, "state.selectedAvatar", "") !== "") ? " btn-start": "")}
                  onClick={() => this.startConversation("Arabic")}
                >
                  دعنا نتحدث!
                </button>

              </div>

              <div className="term-text">
                <p className="font-arabic-light">
                إذا اخترت المتابعة فإنك تؤكد موافقتك على الالتزام بهذه{" "}
                  <a href="https://www.medcare.ae/ar/contents/view/terms-and-conditions.html" target="_blank" rel="noopener noreferrer">الشروط والأحكام</a>
                </p>
                <p>
                  By clicking on ‘let’s talk’, you agree to our{" "}
                  <a href={ENVIORMENT.policy_url} target="_blank" rel="noopener noreferrer">Terms and Conditions</a>
                </p>
              </div>
            </div>
            <PoweredBy className="powerd-by-container" />
          </div>
            

          </div>
        </div>
      </div>
    );
  }
}

export default LayoutTwo;
