import React from "react";
import _ from "lodash";
import './initial-screen.scss';

import StartScreen from "./../StartScreen/StartScreen";
import SplashScreen from "./SplashScreen";

class InitialScreen extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      onBoarding: this.isOnBoardingProcess()
    };
    this.startConversation = this.startConversation.bind(this);
    this.endConversation = this.endConversation.bind(this);
    this.completeonBoarding = this.completeonBoarding.bind(this);
  }
  
  isOnBoardingProcess() {
    if (this.getCookie("onBoarding")) {
      return false;
    } 
    return true;
  }

  componentDidUpdate() {}

  startConversation(selectedAvatar, displayAvatar, lang = "English") {
    this.props.loadConversationScreen(selectedAvatar, displayAvatar);
    this.props.toggleRTL(lang);
  }
  
  endConversation() {
    this.props.endConversation();
  }

  completeonBoarding() {
    this.setState({
      onBoarding: false
    }, () => {
      this.setCookie("onBoarding", 1, 100);
    });
  }

  getCookie(cname) {
    var name = cname + "=";
    var decodedCookie = decodeURIComponent(document.cookie);
    var ca = decodedCookie.split(";");
    for (var i = 0; i < ca.length; i++) {
      var c = ca[i];
      while (c.charAt(0) === " ") {
        c = c.substring(1);
      }
      if (c.indexOf(name) === 0) {
        return c.substring(name.length, c.length);
      }
    }
    return "";
  }

  setCookie(cname, cvalue, exdays) {
    var d = new Date();
    d.setTime(d.getTime() + exdays * 24 * 60 * 60 * 1000);
    var expires = "expires=" + d.toUTCString();
    document.cookie = cname + "=" + cvalue + ";" + expires + ";path=/";
  }

  render() {
    if (_.get(this, "state.onBoarding", true)) {
      return <SplashScreen  completeonBoarding={this.completeonBoarding}  />;
    }
    return <StartScreen  startConversation={this.startConversation}  />;
  }
}
export default InitialScreen;
