import React from 'react';
import "./comprehensive.scss";

// slider
import Slider from "react-slick";
import "slick-carousel/slick/slick.css"; 
import "slick-carousel/slick/slick-theme.css";


//images
import tablelogonasco from "./../../public/images/table_logo_nasco.svg";
import tablelogowatania from "./../../public/images/table_logo_watania.svg";
import tablelogosalama from "./../../public/images/table_logo_salama.svg";
import tableYes from "./../../public/images/table_yes.svg";
import tableNo from "./../../public/images/tabel_no.svg";

class Comprehensive extends React.Component {
  render() {
    const settings = {
      arrows: true,
      dots: false,
      infinite: false,
      speed: 1000,
      focusOnSelect: false,
      swipeToSlide: false,
    };

    return (
         
      <span className="cards-comprehensive-main">
        <Slider {...settings}>
          <div>
            <div className="cards-comprehensive"> 
              <table className="comprehensive-table table">
          <thead>
          <tr>
            <th>Comprehensive</th>
            <th><img src={tablelogonasco} alt="" /></th>
          </tr>
        </thead>
          <tbody>
          <tr>
            <td>Personal Accident Benefits for Driver</td>
            <td className="text-center" width="60px"><img src={tableYes} alt="" /></td>
          </tr>
          <tr>
            <td>Personal Accident Benefits for Passengers</td>
            <td className="text-center" width="60px"><img src={tableYes} alt="" /></td>
          </tr>
          <tr>
            <td>IMC Roadside Assistance - Gold Card</td>
            <td className="text-center" width="60px"><img src={tableYes} alt="" /></td>
          </tr>
          <tr>
            <td>Rent-a-Car Option</td>
            <td className="text-center" width="60px"><img src={tableYes} alt="" /></td>
          </tr>
          <tr>
            <td>Agency Repairs</td>
            <td className="text-center" width="60px"><img src={tableYes} alt="" /></td>
          </tr>
          <tr>
            <td>Off Road Cover</td>
            <td className="text-center" width="60px"><img src={tableYes} alt="" /></td>
          </tr>
          <tr>
            <td>Territorial Limits</td>
            <td className="text-center" width="60px"><img src={tableYes} alt="" /></td>
          </tr>
          <tr>
            <td>Third Party Bodily Injury</td>
            <td className="text-center" width="60px"><img src={tableYes} alt="" /></td>
          </tr>
          <tr>
            <td>Third Party Property Damage</td>
            <td className="text-center" width="60px"><img src={tableYes} alt="" /></td>
          </tr>
          <tr>
            <td>Ambulance Cover</td>
            <td className="text-center" width="60px"><img src={tableYes} alt="" /></td>
          </tr>

          <tr>
            <td>Windscreen Claims</td>
            <td className="text-center" width="60px"><img src={tableYes} alt="" /></td>
          </tr>

          <tr>
            <td>Medical Expenses</td>
            <td className="text-center" width="60px"><img src={tableYes} alt="" /></td>
          </tr>

          <tr>
            <td>Personal Effects</td>
            <td className="text-center" width="60px"><img src={tableYes} alt="" /></td>
          </tr>

          <tr>
            <td>Natural Calamities</td>
            <td className="text-center" width="60px"><img src={tableYes} alt="" /></td>
          </tr>

          <tr>
            <td className="text-bold">TOTAL PRICE (AED)</td>
            <td className="text-bold text-center" width="60px">5,500/-</td>
          </tr>
          <tr className="d-none">
            <td colSpan="2"><div className="cards-comprehensive-btn"><span className="btn btn-blue btn-block">CONFIRM PURCHASE</span></div></td>
          </tr>
        </tbody>        
        </table>
            </div>
          </div>
          <div>
            <div className="cards-comprehensive"> 
              <table className="comprehensive-table table">
          <thead>
          <tr>
            <th>Comprehensive</th>
            <th><img src={tablelogowatania} alt="" /></th>
          </tr>
        </thead>
          <tbody>
          <tr>
            <td>Personal Accident Benefits for Driver</td>
            <td className="text-center" width="60px"><img src={tableYes} alt="" /></td>
          </tr>
          <tr>
            <td>Personal Accident Benefits for Passengers</td>
            <td className="text-center" width="60px"><img src={tableYes} alt="" /></td>
          </tr>
          <tr>
            <td>IMC Roadside Assistance - Gold Card</td>
            <td className="text-center" width="60px"><img src={tableYes} alt="" /></td>
          </tr>
          <tr>
            <td>Rent-a-Car Option</td>
            <td className="text-center" width="60px"><img src={tableYes} alt="" /></td>
          </tr>
          <tr>
            <td>Agency Repairs</td>
            <td className="text-center" width="60px"><img src={tableYes} alt="" /></td>
          </tr>
          <tr>
            <td>Off Road Cover</td>
            <td className="text-center" width="60px"><img src={tableYes} alt="" /></td>
          </tr>
          <tr>
            <td>Territorial Limits</td>
            <td className="text-center" width="60px"><img src={tableYes} alt="" /></td>
          </tr>
          <tr>
            <td>Third Party Bodily Injury</td>
            <td className="text-center" width="60px"><img src={tableYes} alt="" /></td>
          </tr>
          <tr>
            <td>Third Party Property Damage</td>
            <td className="text-center" width="60px"><img src={tableYes} alt="" /></td>
          </tr>
          <tr>
            <td>Ambulance Cover</td>
            <td className="text-center" width="60px"><img src={tableYes} alt="" /></td>
          </tr>

          <tr>
            <td>Windscreen Claims</td>
            <td className="text-center" width="60px"><img src={tableNo} alt="" /></td>
          </tr>

          <tr>
            <td>Medical Expenses</td>
            <td className="text-center" width="60px"><img src={tableNo} alt="" /></td>
          </tr>

          <tr>
            <td>Personal Effects</td>
            <td className="text-center" width="60px"><img src={tableNo} alt="" /></td>
          </tr>

          <tr>
            <td>Natural Calamities</td>
            <td className="text-center" width="60px"><img src={tableNo} alt="" /></td>
          </tr>

          <tr>
            <td className="text-bold">TOTAL PRICE (AED)</td>
            <td className="text-bold text-center" width="60px">4,500/-</td>
          </tr>
          <tr className="d-none">
            <td colSpan="2"><div className="cards-comprehensive-btn"><span className="btn btn-blue btn-block">CONFIRM PURCHASE</span></div></td>
          </tr>
        </tbody>        
        </table>
            </div>
          </div>
          <div>
            <div className="cards-comprehensive"> 
              <table className="comprehensive-table table">
          <thead>
          <tr>
            <th>Comprehensive</th>
            <th><img src={tablelogosalama} alt="" /></th>
          </tr>
        </thead>
          <tbody>
          <tr>
            <td>Personal Accident Benefits for Driver</td>
            <td className="text-center" width="60px"><img src={tableYes} alt="" /></td>
          </tr>
          <tr>
            <td>Personal Accident Benefits for Passengers</td>
            <td className="text-center" width="60px"><img src={tableYes} alt="" /></td>
          </tr>
          <tr>
            <td>IMC Roadside Assistance - Gold Card</td>
            <td className="text-center" width="60px"><img src={tableYes} alt="" /></td>
          </tr>
          <tr>
            <td>Rent-a-Car Option</td>
            <td className="text-center" width="60px"><img src={tableYes} alt="" /></td>
          </tr>
          <tr>
            <td>Agency Repairs</td>
            <td className="text-center" width="60px"><img src={tableYes} alt="" /></td>
          </tr>
          <tr>
            <td>Off Road Cover</td>
            <td className="text-center" width="60px"><img src={tableYes} alt="" /></td>
          </tr>
          <tr>
            <td>Territorial Limits</td>
            <td className="text-center" width="60px"><img src={tableYes} alt="" /></td>
          </tr>
          <tr>
            <td>Third Party Bodily Injury</td>
            <td className="text-center" width="60px"><img src={tableYes} alt="" /></td>
          </tr>
          <tr>
            <td>Third Party Property Damage</td>
            <td className="text-center" width="60px"><img src={tableYes} alt="" /></td>
          </tr>
          <tr>
            <td>Ambulance Cover</td>
            <td className="text-center" width="60px"><img src={tableYes} alt="" /></td>
          </tr>

          <tr>
            <td>Windscreen Claims</td>
            <td className="text-center" width="60px"><img src={tableNo} alt="" /></td>
          </tr>

          <tr>
            <td>Medical Expenses</td>
            <td className="text-center" width="60px"><img src={tableNo} alt="" /></td>
          </tr>

          <tr>
            <td>Personal Effects</td>
            <td className="text-center" width="60px"><img src={tableNo} alt="" /></td>
          </tr>

          <tr>
            <td>Natural Calamities</td>
            <td className="text-center" width="60px"><img src={tableNo} alt="" /></td>
          </tr>

          <tr>
            <td className="text-bold">TOTAL PRICE (AED)</td>
            <td className="text-bold text-center" width="60px">4,700/-</td>
          </tr>
          <tr className="d-none">
            <td colSpan="2"><div className="cards-comprehensive-btn"><span className="btn btn-blue btn-block">CONFIRM PURCHASE</span></div></td>
          </tr>
        </tbody>        
        </table>
            </div>
          </div>
        </Slider>
        </span>
      
    );
  }
}

export default Comprehensive;
