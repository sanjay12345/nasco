import React from "react";
import _ from "lodash";
import "./start-screen.scss";
// images

import coverphoto from "./../../public/images/start/coverphoto.svg";
import backIc from "./../../public/images/start/back.svg";
import ratingimge from "./../../public/images/start/star.svg";
import botimg from "./../../public/images/start/botimg.svg";

class StartScreen extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      getStarted: false
    };
  }
  componentDidMount() {}

  makeStart() {
    this.setState({
      getStarted: true
    });
  }

  startConversation() {
    this.props.startConversation();
  }

  render() {
    const isGetStartedScreen = _.get(this, "state.getStarted", false);
    return (
      <div className={"start-converstion"}>
        <div className="row">
          {/* start screen start */}
          <div className={"col-md "+ (!isGetStartedScreen ? "" : "hide")}>
            <div className="header-transperent">
              <div className="righticon">
                <span className="menu-icon">
                  <span className="menu-icon__text" />
                </span>
              </div>
            </div>
            <div className="cover-photo">
              <img src={coverphoto} alt="" />
            </div>

            <div className="start-links">
              <div className="ratings">
                <span className="ratings-star">
                  <img src={ratingimge} alt="" />
                </span>
                <span className="ratings-counter px-2">(427)</span>
              </div>
              <h1>Hi, I’m Bot Granny. I’m here to help you.</h1>
              <p>
                I can assist you with anything, from quotes to claims, all within a few clicks.
              </p>
              <div className="quicklinks">
                <ul>
                  <li onClick ={ () => {this.makeStart()}}>
                    <span>Get Quick Quotes</span>
                  </li>
                  <li onClick ={ () => {this.makeStart()}}>
                    <span>Manage my Account</span>
                  </li>
                </ul>
              </div>
            </div>
          </div>
          {/* start screen end */}

          {/* start screen start */}
          <div className={"col-md "+ (isGetStartedScreen ? "" : "hide")}>
            <div className="header-transperent">
              <div className="lefticon backIc">
                <span>
                  <img src={backIc} alt="" />
                </span>
              </div>
              <div className="centetitle text-left">Get Quick Quotes</div>
              <div className="righticon">
                <span href="#" className="menu-icon">
                  <span className="menu-icon__text" />
                </span>
              </div>
            </div>
            <div className="assistance">
              <div className="assistance-img">
                <img src={botimg} alt="" />
              </div>
              <div className="assistance-name">
              Granny
                <div className="assistance-ratings-star">
                  <img src={ratingimge} alt="" />
                </div>
                <div className="assistance-type">
                  Personal Insurance Assistant
                </div>
                <div className="assistance-text-content">
                I’ll make sure you find exactly what you need and more importantly, I’ll ensure you don’t pay an extra penny.
                </div>
              </div>
            </div>
           
          </div>
          <div className={"bottom-btn "+ (isGetStartedScreen ? "" : "hide")}>
              <span className="btn btn-green btn-block" onClick ={ () => {this.startConversation()}}>
                GET STARTED
              </span>
            </div>
          
          {/* start screen end */}
        </div>
      </div>
    );
  }
}

export default StartScreen;
