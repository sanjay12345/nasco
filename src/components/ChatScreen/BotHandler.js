import React from "react";
import _ from "lodash";
import $ from 'jquery';


import { ENVIORMENT } from "./../../env";
// import FormGenerate from "./../FormGenerate/FormGenerate";
import Slider from "./../Slider/Slider";
import AlAlain from "./../Forms/AlAlain";
import Autosuggest from "./Autosuggest";
import List from "./List";
import GetUserLocation from "./GetUserLocation";
import MultiSelect from "./MultiSelect";
import RangeSlider from "./../RangeSlider/RangeSlider";
import IdentityVerifySelection from "./../IdentityVerifySelection/identity_verify_selection";
import Comprehensive from "./../Comprehensive";
import QuoteRequest from "../Forms/QuoteRequest";
import CollectEmail from "./../Forms/CollectEmail";


import "./quick-reply.scss";


class BotHandler extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      activeIndex: -1,
      isMapOpen: false
    };
    this.formReply = this.formReply.bind(this);
    this.multiLangQuickReply = this.multiLangQuickReply.bind(this);
    this.userFormReply = this.userFormReply.bind(this);
    this.changeLanguage = this.changeLanguage.bind(this);
  }

  componentDidMount() {}

  componentDidUpdate() {}

  formReply(formValue) {
    console.log(formValue);
    this.props.sendQuickReply(formValue);
  }

  multiLangQuickReply(formValue) {
    this.props.sendMultiLangQuickReply(formValue);
  }

  userFormReply(formValue) {
    this.props.sendQuickReply(formValue, false);
  }

  downloadFile(request) {
    const url = ENVIORMENT.download_url + "?filename=" + request.filename;
    return (
      <div className="bot">
        <div className="bot-avatar">
          <img
            src={require("./../../public/images/bot-avatar-" +
              _.get(this, "props.avatar", "") +
              ".png")}
            alt=""
          />
          <span className="status-online"> </span>
        </div>
        <div className="bot-msg">
          <span className="float-left" style={{ clear: "both" }} />
          <div>
            <span className="float-left" style={{ clear: "both" }} />
            <button
              className="btn btn-sm m-1 btn-border-bbl pull-right"
              onClick={event => {
                event.preventDefault();
                window.open(url);
              }}
            >
              Download
            </button>
          </div>
        </div>
      </div>
    );
  }

  openMap(url) {
    return (
      <div className="bot">
        <div className="bot-avatar">
          <img
            src={require("./../../public/images/bot-avatar-" +
              _.get(this, "props.avatar", "") +
              ".png")}
            alt=""
          />
          <span className="status-online"> </span>
        </div>
        <div className="bot-msg">
          <span className="float-left" style={{ clear: "both" }} />
          <div>
            <span className="float-left" style={{ clear: "both" }} />
            <button
              className="btn btn-sm m-1 btn-border-bbl pull-right"
              onClick={event => {
                event.preventDefault();
                window.open(url);
              }}
            >
              Open Direction Map
            </button>
          </div>
        </div>
      </div>
    );
  }

  changeLanguage(lang) {
    this.props.changeLanguage(lang);
  }

  loadBigAvatar() {
    return (
      <div className="load_avtar">
        {/* <img
          src={require(`./../../public/images/big-avatar/${_.get(
            this,
            "props.avatar",
            ""
          )}.png`)}
          alt=""
        /> */}
      </div>
    );
  }

  loadImage() {
    const project = ENVIORMENT.project;
    return (
      <div className="bot mb-2">
        <div className="bot-msg">
          <img
            src={
              _.get(this, "props.chat.data[0].image_type", "") === "name"
                ? require(`./../../public/images/image-component/${project}/${_.get(
                    this,
                    "props.chat.data[0].image",
                    ""
                  )}`)
                : _.get(this, "props.chat.data[0].image", "")
            }
            alt=""
            className="general-img"
          />
        </div>
      </div>
    );
  }

  loadInvoiceImage() {
    return (
      <div className="bot mb-2">
        <div className="bot-msg">
          <img src={require(`./../../public/images/invoice-img.jpg`)} alt="" className="invoice-img"  />
        </div>
      </div>
    );
  }


  hideConfirmationPopup() {
    $("#confirmationPopup").addClass("d-none").removeClass("confirmationpopup");
  }

  confirmationPopup() {
    return <div id="confirmationPopup" className="modal confirmationpopup" onClick={this.hideConfirmationPopup}>
      <div className="modal-dialog modal-dialog-centered">
        <div className="modal-content">
          <div className="modal-header">
            <button type="button" className="close" data-dismiss="modal">&times;</button>
          </div>
          <div className="modal-body">
            <p>Your email has been sent successfully.</p>
          </div>
        </div>      
      </div>
    </div>;
  }

  removeGmap() {
    try {
      if (document.getElementById("openGMap")) {
        setTimeout(document.getElementById("openGMap").remove(), 500);
      }
    } catch (e) {}
  }

  render() {
    if (_.get(this, "props.chat.type", "") === "get_user_location") {
      return (
        <GetUserLocation
          chat={this.props.chat.content}
          userFormReply={this.userFormReply}
          formReply={this.formReply}
          isRTLFlow={_.get(this, "props.isRTLFlow", false)}
          lang={_.get(this, "props.lang", "English")}
        />
      );
    }

    if (_.get(this, "props.chat.type", "") === "download") {
      return this.downloadFile(_.get(this, "props.chat.data[0]"));
    }

    if (_.get(this, "props.chat.type", "") === "image") {
      return this.loadImage();
    }

    if (_.get(this, "props.chat.type", "") === "invoice_display") {
      return this.loadInvoiceImage();
    }

    if (_.get(this, "props.chat.type", "") === "comprehensive") {
      return <Comprehensive />;
    }

    if (_.get(this, "props.chat.type", "") === "quote_request") {
      return <QuoteRequest userFormReply={this.userFormReply} />;
    }
    
    if (_.get(this, "props.chat.type", "") === "confimation_popup") {
      return this.confirmationPopup();
    }

    if (_.get(this, "props.chat.type", "") === "identity_verify_selection") {
      return <IdentityVerifySelection userFormReply={this.userFormReply} mode={"person"} />
    }

    if (_.get(this, "props.chat.type", "") === "car_verify_selection") {
      return <IdentityVerifySelection userFormReply={this.userFormReply} mode={"car"} />
    }

    if (_.get(this, "props.chat.type", "") === "collect_email") {
      return <CollectEmail mode={"email"} userFormReply={this.userFormReply} />
    }

    if (_.get(this, "props.chat.type", "") === "register_account") {
      return <CollectEmail mode={"register_account"} userFormReply={this.userFormReply} />
    }

    if (_.get(this, "props.chat.type", "") === "get_map_direction") {
      return (
        <button
          ref={el => {
            this.openMap = el;
          }}
          id="openGMap"
          onClick={event => {
            event.preventDefault();
            if (!this.state.isMapOpen) {
              this.setState(
                {
                  isMapOpen: true
                },
                () => {
                  const url =
                    "https://www.google.com/maps/dir/" +
                    _.get(this, "props.chat.data[0].from.lat", "") +
                    "," +
                    _.get(this, "props.chat.data[0].from.lng", "") +
                    "/" +
                    _.get(this, "props.chat.data[0].to.lat", "") +
                    "," +
                    _.get(this, "props.chat.data[0].to.lng", "") +
                    "/";
                  window.open(url);
                }
              );
            }

            let element = document.getElementById("openGMap");
            element.disbled = false;
          }}
          className="hide"
        >
          {setTimeout(function() {
            try {
              if (document.getElementById("openGMap")) {
                document.getElementById("openGMap").click();
              }
            } catch (e) {}
          }, 250)}
        </button>
      );
    }

    if (_.get(this, "props.chat.type", "") === "list") {
      return (
        <List
          chat={this.props.chat}
          formReply={this.formReply}
          multiLangQuickReply={this.multiLangQuickReply}
          isRTLFlow={_.get(this, "props.isRTLFlow", false)}
          lang={_.get(this, "props.lang", "English")}
        />
      );
    }

    if (_.get(this, "props.chat.type", "") === "search_bar") {
      return (
        <Autosuggest
          chat={this.props.chat}
          formReply={this.formReply}
          isRTLFlow={_.get(this, "props.isRTLFlow", false)}
          lang={_.get(this, "props.lang", "English")}
        />
      );
    }

    // if (
    //   _.get(this, "props.chat.type", "") === "otp" ||
    //   _.get(this, "props.chat.type", "") === "date"
    // ) {
    //   return (
    //     <FormGenerate
    //       chat={this.props.chat}
    //       formReply={this.formReply}
    //       isRTLFlow={_.get(this, "props.isRTLFlow", false)}
    //       multiLangQuickReply={this.multiLangQuickReply}
    //       lang={_.get(this, "props.lang", "English")}
    //     />
    //   );
    // }

    // if (
    //   _.get(this, "props.chat.type", "") === "new_user_form" ||
    //   _.get(this, "props.chat.type", "") === "existing_user_form" ||
    //   _.get(this, "props.chat.type", "") === "auth_form"
    // ) {
    //   return (
    //     <FormGenerate
    //       chat={this.props.chat}
    //       userFormReply={this.userFormReply}
    //       isRTLFlow={_.get(this, "props.isRTLFlow", false)}
    //       lang={_.get(this, "props.lang", "English")}
    //     />
    //   );
    // }

    if (
      _.get(this, "props.chat.type", "") === "payment_request" ||
      _.get(this, "props.chat.type", "") === "edit_contract_form"
    ) {
      return <AlAlain chat={_.get(this, "props.chat", {})} userFormReply={this.userFormReply} />;
    }

    if (_.get(this, "props.chat.type", "") === "carousel") {
      return (
        <Slider
          sliderData={_.get(this, "props.chat", [])}
          multiLangQuickReply={this.multiLangQuickReply}
          isRTLFlow={_.get(this, "props.isRTLFlow", false)}
          lang={_.get(this, "props.lang", "English")}
        />
      );
    }

    if (_.get(this, "props.chat.type", "") === "range_slider") {
      return (
        <RangeSlider
          chat={_.get(this, "props.chat", {})}
          formReply={this.formReply}
        />
      );
    }

    if (_.get(this, "props.chat.type", "") === "load_big_avatar") {
      return this.loadBigAvatar();
    }
  
    if (_.get(this, "props.chat", "") === "") {
      return false;
    }
    
    return (
      <MultiSelect
        chat={_.get(this, "props.chat")}
        avatar={_.get(this, "props.avatar", "")}
        userFormReply={this.userFormReply}
        formReply={this.formReply}
        changeLanguage={this.changeLanguage}
        multiLangQuickReply={this.multiLangQuickReply}
        isRTLFlow={_.get(this, "props.isRTLFlow", false)}
        lang={_.get(this, "props.lang", "English")}
      />
    );
  }
}

export default BotHandler;
