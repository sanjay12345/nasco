import React from 'react';
// import { GoogleApiWrapper, Map, InfoWindow, Marker } from "google-maps-react";
import {geolocated} from 'react-geolocated';


interface IDemoProps {
  label: string;
}

class MyLocation extends React.Component<IDemoProps & GeolocatedProps> {

	constructor(props) {
	    super(props);
	    this.state = {
	      latitude: '',
	      longitude: '',
	    };
	}
	// {this.setState({latitude: this.props.coords.latitude, longitude: this.props.coords.longitude})}
	render() {
	  	// console.log('location-------');
	  	// console.log(this.props)
	    return !this.props.isGeolocationAvailable
	      ? <div>Your browser does not support Geolocation</div>
	      : !this.props.isGeolocationEnabled
	        ? <div>Geolocation is not enabled</div>
	        : this.props.coords
	          ? <div className="map-lat-lng">
		          	<p>latitude : {this.props.coords.latitude} </p>
		            <p>longitude : {this.props.coords.longitude}</p>
		        </div>
	          : <div>Getting the location data&hellip; </div>;
	   	// return this.props.coords;
  	}
}

export default geolocated({
  positionOptions: {
    enableHighAccuracy: false,
  },
  userDecisionTimeout: 5000,
})(MyLocation);