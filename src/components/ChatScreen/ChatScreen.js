import React from "react";
import _ from "lodash";
// config
import { ENVIORMENT } from "./../../env";
import { guid } from "./../utils/Utility";
import { Translate } from "./../Language/Translate";


// Child components
import UserReply from "./UserReply";
import BotHandler from "./BotHandler";

// Css loadingtoggleRTL
import "./chat-screen.scss";
// images
 import headerLogo  from "./../../public/images/logo.svg";



class ChatScreen extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      is_bot_visible: false,
      is_conversation_start: false,
      msg: "",
      txtMsg: "",
      chatlogs: [],
      user: this.generateGuid(),
      channel: ENVIORMENT.channel,
      ws: new WebSocket(ENVIORMENT.socketUrl),
      attempts: 1,
      is_bot_typing: false,
      isVisibleHumberMenu: false,
      input_disable: false
    };
    this.sendQuickReply = this.sendQuickReply.bind(this);
    this.changeLanguage = this.changeLanguage.bind(this);
    this.sendMultiLangQuickReply = this.sendMultiLangQuickReply.bind(this);
  }

  toggleHumberMenu() {
    this.setState({
      isVisibleHumberMenu: !this.state.isVisibleHumberMenu
    });
  }

  generateInterval(k) {
    if (this.props.reconnectIntervalInMilliSeconds > 0) {
      return this.props.reconnectIntervalInMilliSeconds;
    }
    return Math.min(30, Math.pow(2, k) - 1) * 1000;
  }
  /* generate of user id start */
  generateGuid() {
    let guId;
    if (this.getCookie("botkit_guid")) {
      guId = guid();
      this.setCookie("botkit_guid", guId, 1);
      return this.getCookie("botkit_guid");
    }
    guId = guid();
    this.setCookie("botkit_guid", guId, 1);
    return guId;
  }

  getCookie(cname) {
    var name = cname + "=";
    var decodedCookie = decodeURIComponent(document.cookie);
    var ca = decodedCookie.split(";");
    for (var i = 0; i < ca.length; i++) {
      var c = ca[i];
      while (c.charAt(0) === " ") {
        c = c.substring(1);
      }
      if (c.indexOf(name) === 0) {
        return c.substring(name.length, c.length);
      }
    }
    return "";
  }

  setCookie(cname, cvalue, exdays) {
    var d = new Date();
    d.setTime(d.getTime() + exdays * 24 * 60 * 60 * 1000);
    var expires = "expires=" + d.toUTCString();
    document.cookie = cname + "=" + cvalue + ";" + expires + ";path=/";
  }
  /* generate of user id end */

  /* setting up web socket start */
  setupWebsocket() {
    let websocket = this.state.ws;
    websocket.onopen = () => {
      const greeting_msg =
        "_hi_" +
        _.get(this, "props.avatar", "") +
        "_" +
        (_.get(this, "props.isRTLFlow", false) ? "Arabic" : "English");
      console.log("Websocket connected", greeting_msg);
      websocket.send(
        JSON.stringify({
          type: "message",
          text: greeting_msg,
          user: _.get(this, "state.user", ""),
          channel: "socket"
        })
      );
    };

    websocket.onmessage = evt => {
      let responseData = JSON.parse(evt.data);
      console.log(evt.data);
      if (
        _.get(responseData, "continue_typing", false) === true ||
        _.get(responseData, "type", "") === "typing"
      ) {
        this.setState({ is_bot_typing: true });
        if (_.get(responseData, "continue_typing", false) === true) {
          this.setState({
            chatlogs: [...this.state.chatlogs, JSON.parse(evt.data)]
          }, ()=> {
            
          });
        }
      } else {
        this.setState({ is_bot_typing: false });
        this.setState({
          chatlogs: [...this.state.chatlogs, JSON.parse(evt.data)]
        }, () => {
          
        });
      }
      if (
        _.get(responseData, "input_disable", false) === true
      ) {
        this.setState({
          input_disable: true
        })
      } else {
        this.setState({
          input_disable: false
        })
      }
      
      this.scrollToBottom();
    };
    websocket.onclose = () => {
      console.log("Websocket disconnected");
      if (typeof this.props.onClose === "function") this.props.onClose();
      if (this.shouldReconnect) {
        let time = this.generateInterval(this.state.attempts);
        this.timeoutID = setTimeout(() => {
          this.setState({ attempts: this.state.attempts + 1 });
          this.setState({
            ws: new WebSocket(this.props.url, this.props.protocol)
          });
          this.setupWebsocket();
        }, time);
      }
    };
  }
  /* setting up web socket end */
  /*
    Send message on enter of textbox or onclick of send button
  */
  sendMessage(e) {
    this.setState({ txtMsg: e.target.value });
    if (e.key === "Enter" && e.target.value !== "") {
      this.sendUserReply(e.target.value);
      this.scrollToBottom();
    }
  }

  sendMessageBtn() {
    if (_.get(this, "state.txtMsg", "") !== "") {
      this.sendUserReply(_.get(this, "state.txtMsg", ""));
      this.scrollToBottom();
    }
  }

  sendUserReply(message) {
    this.setState({ msg: message , txtMsg: "" });

    let chatlogs = this.state.chatlogs;
    chatlogs.push({
      is_received: false,
      text: message
    });
    this.setState({
      chatlogs: chatlogs
    });
    let websocket = this.state.ws;
    console.log("request", message);
    websocket.send(
      JSON.stringify({
        type: "message",
        text: message,
        user: _.get(this, "state.user", ""),
        channel: "socket"
      })
    );
    this.scrollToBottom();
  }

  /* send multi language quick reply */
  sendMultiLangQuickReply(quickreply, displayReply = true) {
    if (displayReply) {
      try {
        quickreply = JSON.parse(quickreply);
      } catch (e) {}
      let displayMessage = "";

      if (Array.isArray(quickreply)) {
        const arr_replies = quickreply;
        arr_replies.map((quickreply, index) => {
          displayMessage = displayMessage + quickreply.title + ", ";
        });
        displayMessage = displayMessage.slice(0, -2);
      } else {
        displayMessage = quickreply.title;
      }
      let chatlogs = _.get(this, "state.chatlogs", []);
      chatlogs.push({
        is_received: false,
        text: displayMessage
      });
      this.setState({
        chatlogs: chatlogs
      });
    }
    console.log(
      "request",

      Array.isArray(quickreply)
        ? JSON.stringify(quickreply)
        : quickreply.payload
    );
    let websocket = this.state.ws;
    websocket.send(
      JSON.stringify({
        type: "message",
        text: Array.isArray(quickreply)
          ? JSON.stringify(quickreply)
          : quickreply.payload,
        user: _.get(this, "state.user", ""),
        channel: "socket"
      })
    );
  }
  /*
    send quick replies list
  */
  sendQuickReply(quickreply, displayReply = true) {
    if (displayReply) {
      let chatlogs = _.get(this, "state.chatlogs", []);
      chatlogs.push({
        is_received: false,
        text: quickreply
      });
      this.setState({
        chatlogs: chatlogs
      });
    }
    console.log("request", quickreply);
    let websocket = this.state.ws;
    websocket.send(
      JSON.stringify({
        type: "message",
        text: quickreply,
        user: _.get(this, "state.user", ""),
        channel: "socket"
      })
    );
  }

  componentDidMount() {
    this.setupWebsocket();
    this.setState({
      chatlogs: [
        // {
        //   is_received: true,
        //   type: "load_big_avatar"
        // }
        // ,{
        //   is_received: true,
        //   type: "image",
        //   "data": {
        //     "image_type": "url",
        //     "image": "https://media.wired.com/photos/5b86fce8900cb57bbfd1e7ee/master/pass/Jaguar_I-PACE_S_Indus-Silver_065.jpg",
        //   }
        // },

        // {
        //   is_received: true,
        //   "text":"Perfect! How are you feeling today?","quick_replies":[{"title":"haha ","payload":"I'm alright"},{"title":"eeeeee 🤒","payload":"Not too good!"}],"channel":"socket","sent_timestamp":1549283151386,"user":"d4cfb5df-1fc7-6534-f980-9f6bd8f7e569","to":"d4cfb5df-1fc7-6534-f980-9f6bd8f7e569","type":"message"
        // },
        // {
        //   is_received: false,
        //   "text":"Perfect! How are you feeling today?","quick_replies":[{"title":"haha ","payload":"I'm alright"},{"title":"eeeeee 🤒","payload":"Not too good!"}],"channel":"socket","sent_timestamp":1549283151386,"user":"d4cfb5df-1fc7-6534-f980-9f6bd8f7e569","to":"d4cfb5df-1fc7-6534-f980-9f6bd8f7e569","type":"message"
        // }

       
        // ,{ is_received: true,
        //   "type":"multi_select",
        //   "text":"I'm trained to help you in both English & Arabic. What would you prefer?",
        //   "quick_replies":[
        //     {"title":"English","payload":"English"},
        //     {"title":"Arabic","payload":"Arabic"},
        //     {"title":"Hindi","payload":"Hindi"},
        //     {"title":"Gujarati","payload":"Gujarati"},
        //     {"title":"Marathi","payload":"Marathi"},
        //   ]
        //   ,
        //   "channel":"socket",
        //   "sent_timestamp":1549017262630,"user":"064db488-99ba-3ee9-3bf7-b4e099645365",
        //   "to":"064db488-99ba-3ee9-3bf7-b4e099645365"
        // }
        //  {
        //   is_received: true,
        //   type: "download",
        //   text: "download",
        //   channel: "socket",
        //   //data:[{"filename": "\\\\10.10.8.23\\files\\0900341580c489567.pdf"}]
        //   data:[{"filename": "http://www.pdf995.com/samples/pdf.pdf"}]
        // }
        // ,{
        //   is_received: true,

        //   "channel": "socket",

        //       type: "get_map_direction",
        //       text: "get_map_direction",
        //       data: [{
        //         from : {
        //           "lat": "18.5918294",
        //           "lng": "73.733393"
        //          },
        //         to: {
        //           "lat": "18.5793481",
        //           "lng": "73.9067281"
        //           }
        //       }]

        // }
        // {
        //   is_received: true,
        //   type: "auth_form",
        //   channel: "socket"
        //  }
        // ,{
        //   is_received: true,
        //   "type": "list",
        //   "channel": "socket",
        //     "data": [
        //     {
        //       "label": "Doctor",
        //       "description": "I know my Doctor.",
        //       "image_type": "name",
        //       "image": "Doctor.jpg",
        //       "buttons": [
        //           { "title": "Doctor", "payload": "Doctor" }
        //         ]
        //     }, {
        //       "label": "Specialisation",
        //       "description": "I know my Specialisation.",
        //       "image_type": "name",
        //       "image": "Specialization.jpg",
        //       "buttons": [
        //           { "title": "Specialization", "payload": "Specialization" }
        //         ]
        //     }, {
        //       "label": "Symptom Analyser",
        //       "description": "I know my Symptom.",
        //       "image_type": "name",
        //       "image": "SymptomAnalyzer.jpg",
        //       "buttons": [
        //           { "title": "SymptomAnalyzer", "payload": "SymptomAnalyzer" }
        //         ]
        //     }
        //   ]
        // }
        //  ,{
        //    is_received: true,
        //    "type":"search_bar","text":"Enter Specialisation","data":["التخدير","ANESTHESIOLOGY","AUDIOLOGY","BARIATRIC SURGERY","CARDIOLOGY","DENTISTRY","DERMATOLOGY","DIET & NUTRITION","E.N.T","EMERGENCY","ENDOCRINOLOGYFAMILY","MEDICINE","FERTILITY","GASTROENTEROLOGY","GENERAL","SURGERY","HEMATOLOGYINTERNAL","MEDICINE","NEONATOLOGY","NEUROLOGY","NEUROSURGERY","OBSTETRICS & GYNECOLOGY","OPHTHALMOLOGY","ORTHOPAEDICS","PAEDIATRICS","PEDIATRIC SURGERY","PHYSIOTHERAPY","PLASTIC SURGERY","PSYCHIATRY","PULMONOLOGY","RADIOLOGY","RHINOPLASTY","UROLOGY"],"channel":"socket","sent_timestamp":1548310688613,"user":"bfacec25-f00a-4ebe-7c9d-bf4066f44d65","to":"bfacec25-f00a-4ebe-7c9d-bf4066f44d65"
        //  }
        // ,{
        //   is_received: true,
        //   type: "auth_form",
        //   channel: "socket"
        // }
        // ,{
        //   is_received: true,
        //   type: "otp",
        //   channel: "socket"
        // }
        // ,{
        //   is_received: true,
        //   type: "date",
        //   text: "date-picker",
        //   channel: "socket",
        //   range: "past"
        // }
        // , { is_received: true,
        //   "type": "carousel",
        //   "text": "You could either pick one of the following or just type out what you're looking for...📝",
        //   "data": [
        //     {
        //       "label": "Manage Appointments",
        //       "description": "You could either pick one of the following or just type out what you're looking for...📝",
        //       "image_type": "name",
        //       "image": "Medcare Medical Centre Motor Cit.png",
        //       "buttons": [
        //         { "title": "Book Appointment", "payload": "Book Appointment" },
        //         { "title": "Reschedule Appointment", "payload": "Reschedule Appointment" },
        //         { "title": "Cancel Appointment", "payload": "Cancel Appointment" }
        //       ]
        //     },
            //  { "label": "Locate a Centre", "description": "", "image_type": "url", "image": "https://www.medcare.ae/public/uploads/doctor/ihab-ramadan.jpg", "buttons": [{ "title": "Find me a centre", "payload": "Find me a centre" }, { "title": "View all centres", "payload": "View all centres" }] }, { "label": "Symptom Analyser", "description": "", "image_type": "name", "image": "SymptomChecker.jpg", "buttons": [{ "title": "Check your Symptoms", "payload": "Check your Symptoms" }] }], "channel": "socket", "sent_timestamp": 1548236243318, "user": "ec7f2c61-3e3c-bf5c-bccf-a67cf3386a07", "to": "ec7f2c61-3e3c-bf5c-bccf-a67cf3386a07" }
        // ,{
        //   is_received: true,
        //   type: "new_user_form",
        //     channel: "socket",
        //     content: {
        //         type: "new_user_form",
        //         text: "Super! I would appreciate it if you could fill out the form (for the patient) below to help me book you in. 😎"
        //     }
        // }
        // ,{
        //   is_received: true,
        //   type: "existing_user_form",
        //   channel: "socket",
        // }
        // , {
        //   is_received: true,
        //   type: "range_slider",
        //   channel: "socket",
        //   input_disable: true,
        //   data: {
        //     min: 0,
        //     max: 10,
        //     step: 2
        //   }
        // }
        // ,{
        //   is_received: true,
        //   type: "identity_verify_selection",
        //   channel: "socket"
        // },
        // ,{
        //   is_received: true,
        //   type: "car_verify_selection",
        //   channel: "socket"
        // },
        // ,{
        //   is_received: true,
        //   type: "edit_contract_form",
        //   channel: "socket",
        // }
        // ,{
        //   is_received: true,
        //   type: "collect_email",
        //   channel: "socket"
        // },
        // ,{
        //   is_received: true,
        //   type: "register_account",
        //   channel: "socket"
        // },
        // ,{
        //   is_received: true,
        //   type: "payment_request",
        //   channel: "socket",
        // }
        // ,{
        //   is_received: true,
        //   type: "comprehensive",
        //   channel: "socket",
        // },
        // {
        //   is_received: true,
        //   type: "quote_request",
        //   channel: "socket",
        // },

        // ,{
        //   is_received: true,
        //   type: "get_user_location",
        //   channel: "socket",
        // }
        // ,{"type":"carousel","text":"profileCarousel","data":[{"label":"Dr Mousumee Nanda","description":"Specialist Dermatology",
        // "image_type":"url","image":"https://www.medcare.ae/public/uploads/doctor/Leo_(3).jpg","buttons":[{"title":"Book Now!","payload":"Book Now"},{"title":"Change Preference","payload":"Change Preference"}]}],"channel":"socket","sent_timestamp":1550578358276,"user":"3f2e79bf-f5af-0d9a-6834-1f65a3cd16fb","to":"3f2e79bf-f5af-0d9a-6834-1f65a3cd16fb"}
        // ,{
        //   "text": "image",
        //   "type": "image",
        //   "data" : [{
        //       "image_type": "name",
        //       "image": "Apple.jpg"
        //   }]
        // }
        // {is_received: true, "type": "carousel", "text": "mainCarousel", "data": [{"buttons": [{"payload": "/intent_insurance_policy{\"insurance policy\": \"motor\"}", "title": "POLICY SERVICES"}, {"payload": "/Intent_Roadside_Assistance", "title": "ROAD ASSISTANCE"}, {"payload": "/Intent_Emergency", "title": "EMERGENCY"}], "description": "", "image": "Motor Insurance.jpg", "image_type": "name", "label": "Motor Insurance"}, {"buttons": [{"payload": "/my policy", "title": "MY POLICY"}, {"payload": "/intent_insurance_policy{\"insurance policy\": \"medical\"}", "title": "GET A POLICY"}, {"payload": "/submit a claim", "title": "SUBMIT A CLAIM"}], "description": "", "image": "Medical Insurance.jpg", "image_type": "name", "label": "Medical Insurance"}, {"buttons": [{"payload": "/intent_insurance_policy{\"insurance policy\": \"property\"}", "title": "POLICY DETAILS"}, {"payload": "/manage account", "title": "MANAGE ACCOUNT"}, {"payload": "/submit a claim", "title": "SUBMIT A CLAIM"}], "description": "", "image": "Property Insurance.jpg", "image_type": "name", "label": "Property Insurance"}], "channel": "socket", "user": "83ac732c-0650-9ee6-9131-0e5978dbbecf", "to": "83ac732c-0650-9ee6-9131-0e5978dbbecf"}
        // ,{
        //     is_received: true,
        //     type: "invoice_display",
        //     channel: "socket",
        //   }
        // ,{
        //     is_received: true,
        //     type: "confimation_popup",
        //     channel: "socket",
        //   }
      ]
    },() => {
      
    });
    this.scrollToBottom();
  }

  /*
    scroll till an end of chat div
  */
  scrollToBottom = () => {
    try {
      this.messagesEnd.scrollIntoView({ behavior: "smooth" });      
    } catch (Exception) {}
  };

  componentDidUpdate() { 
    this.scrollToBottom();
  }

  generateChatHistory(chat, i) {
    if (_.get(chat, "is_received", true) === true) {
      return (
        <BotHandler
          key={i}
          sendQuickReply={this.sendQuickReply}
          sendMultiLangQuickReply={this.sendMultiLangQuickReply}
          avatar={_.get(this, "props.avatar", "")}
          chat={chat}
          chatIndex={i}
          changeLanguage={this.changeLanguage}
          isRTLFlow={_.get(this, "props.isRTLFlow", false)}
          lang={_.get(this, "props.lang", "English")}
        />
      );
    } else {
      return (
        <UserReply
          key={i}
          chat={chat}
          isRTLFlow={_.get(this, "props.isRTLFlow", false)}
          lang={_.get(this, "props.lang", "English")}
        />
      );
    }    
  }

  resetChatSession() {
    this.setState({
        chatlogs: [],
        isVisibleHumberMenu: false
      }, () => {
        this.sendMessageToBot("/restart");
        const greeting_msg =
          "_hi_" +
          _.get(this, "props.avatar", "") +
          "_" +
          (_.get(this, "props.isRTLFlow", false) ? "Arabic" : "English");

        setTimeout(() => {
          this.sendMessageToBot(greeting_msg);
        }, 500);
      }
    );
  }

  endChatSession() {
    this.setState({
        chatlogs: [],
        isVisibleHumberMenu: false
      }, () => {
        this.sendMessageToBot("end");
        this.props.endConversation();
    });
  }

  sendMainMenu() {
    this.sendMessageToBot("/main_menu");
    this.setState({
      isVisibleHumberMenu: false
    });
  }

  sendMessageToBot(message) {
    let websocket = this.state.ws;
    console.log("request", message);
    websocket.send(
      JSON.stringify({
        type: "message",
        text: message,
        user: _.get(this, "state.user", ""),
        channel: "socket"
      })
    );
  }

  changeLanguage(lang) {
    this.props.toggleRTL(lang);
  }

  render() {
    const isBotTyping = _.get(this, "state.is_bot_typing", false);
    const isInputDisable = _.get(this, "state.input_disable", false);
    const chatlogs = _.get(this, "state.chatlogs", []);
    return (
      <div className="chat-converstion">
        <div className="container">
          <div className="row">
            {/* <div className="chat-box-header position-relative">
              <div className="float-left headeravtar">
                <img
                  src={require("./../../public/images/bot-avatar-" +
                    _.get(this, "props.avatar", "") +
                    ".png")}
                  alt=""
                />
              </div>
              <div className="col bot-name">
                {_.get(this, "props.avatarDispName", "")}
              </div>
              <span className="box-header-ic">
                <span
                  className="btn-link"
                  onClick={() => this.props.toggleBotButton()}
                >
                  <img src={collapsclose} alt="" />
                </span>
              </span>
            </div> */}


            <div className="header-transperent header-blue">
                <div className="header-logo">
                <span><img src={headerLogo} alt=""/></span>
                </div>
                <div className="centetitle text-left">
                    Get Quick Quotes
                </div>
                <div className="righticon">
                  <span href="#" className="menu-icon" onClick={() => this.toggleHumberMenu()}><span className="menu-icon__text"></span></span>
                </div>




                <div
                  className={
                    "menu-pops arrow_box " +
                    (!_.get(this, "state.isVisibleHumberMenu", false)
                      ? "hide"
                      : "")
                  }
                >
                  <h4 className={"p-2"+((_.get(this, "props.isRTLFlow", false)) ? (" font-arabic ") : (""))}>
                    {Translate("MAIN", _.get(this, "props.lang"))}
                  </h4>
                  <ul>
                    <li>
                      <span className={"p-2"+((_.get(this, "props.isRTLFlow", false)) ? (" font-arabic ") : (""))} onClick={() => this.sendMainMenu()}>
                        {Translate("MAIN_MENU", _.get(this, "props.lang"))}
                      </span>
                    </li>
                    <li>
                      <span
                        className={"p-2"+((_.get(this, "props.isRTLFlow", false)) ? (" font-arabic ") : (""))}
                        onClick={() => this.resetChatSession()}
                      >
                        {Translate(
                          "RESTART_SESSION",
                          _.get(this, "props.lang")
                        )}
                      </span>
                    </li>                
                  </ul>
                </div>


            </div>


            <div className="chat-body">
              <div className="chat-logs">
                {chatlogs.map((chat, i) => this.generateChatHistory(chat, i))}    
                <div
                  style={{ float: "left", clear: "both" }}
                  ref={el => {
                    this.messagesEnd = el;
                  }}
                >
                </div>            
              </div>
              
              <div className="bottom-part">
                <div className="bot-typeing">
                  {isBotTyping && (
                    <div className="ml-1 typing-indicator">
                      <div className={"bot-typer-name"+((_.get(this, "props.isRTLFlow", false)) ? (" font-arabic-light ") : (""))}>
                        {_.get(this, "props.avatarDispName", "")}{" "}
                        {Translate("IS_TYPING", _.get(this, "props.lang"))}
                      </div>
                      <span />
                      <span />
                      <span />
                    </div>
                  )}
                </div>

                <div className="type-msg mb-1">
                  <div className="input-msg-write">
                    <input
                      type="text"
                      className="mb-2 ml-2"
                      placeholder={Translate(
                        "TYPE_A_MESSAGE",
                        _.get(this, "props.lang", "English")
                      )}
                      autoComplete="off"
                      value={_.get(this, "state.txtMsg", "")}
                      onChange={e => this.sendMessage(e)}
                      onKeyPress={e => this.sendMessage(e)}
                      disabled={
                        (_.get(this, "props.isRTLFlow", false) || isInputDisable)
                         ? "disabled" : ""
                      }
                    />
                    <button
                      className="btn-link btn-send"
                      type="button"
                      onClick={e => this.sendMessageBtn(e)}
                    >
                      {_.get(this, "props.isRTLFlow", false) && (
                        <img
                          src={require(`./../../public/images/send-reverse.svg`)}
                          alt=""
                        />
                      )}
                      {ENVIORMENT.project === "al-ain" &&
                        _.get(this, "props.isRTLFlow", false) === false && (
                          <img
                            src={require(`./../../public/images/send.svg`)}
                            alt=""
                          />
                        )}
                      {ENVIORMENT.project === "medcare" &&
                        _.get(this, "props.isRTLFlow", false) === false && (
                          <img
                            src={require(`./../../public/images/send-medcare.svg`)}
                            alt=""
                          />
                        )}
                    </button>
                  </div>
                </div>
                
              </div>
            </div>
          </div>
        </div>
      </div>
    );
  }
}

export default ChatScreen;
