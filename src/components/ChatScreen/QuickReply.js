import React from "react";
import botAvatar from "./../../public/images/bot-avatar.png";

class QuickReply extends React.Component {
  render() {
    return (
      <div className="bot">
        <div className="bot-avatar">
          <img src={botAvatar} alt="" />
        </div>
        <div className="bot-msg">
          <p>{this.props.chat.msg}</p>
        </div>
      </div>
    );
  }
}

export default QuickReply;
