import React from 'react';
import ReactDOM from "react-dom";
import { GoogleApiWrapper } from "google-maps-react";
// import {geolocated} from 'react-geolocated';
// import MyLocation from "./MyLocation";
// import { compose, withProps } from "recompose";
// import { withScriptjs, withGoogleMap, GoogleMap, Marker, lifecycle } from "react-google-maps"

export class Search extends React.Component {
    
    initMap() {
      var directionsService = new google.maps.DirectionsService;
      var directionsDisplay = new google.maps.DirectionsRenderer;
      var map = new google.maps.Map(document.getElementById('map'), {
        zoom: 7,
        center: {lat: 41.85, lng: -87.65}
      });
      directionsDisplay.setMap(map);

      var onChangeHandler = function() {
        this.calculateAndDisplayRoute(directionsService, directionsDisplay);
      };
    }

    calculateAndDisplayRoute(directionsService, directionsDisplay) {
      directionsService.route({
        origin: document.getElementById('start').value,
        destination: document.getElementById('end').value,
        travelMode: 'DRIVING'
      }, function(response, status) {
        if (status === 'OK') {
          directionsDisplay.setDirections(response);
        } else {            
          window.alert('Directions request failed due to ' + status);
        }
      });
  }
  render() {
    console.log('map-------');
    // console.log(this.props);
    // console.log(this.state);
    return (
        <div id="search">
          <input id="start" onChange={this.onChangeHandler} value="asdasd" />
          <input id="end" onChange={this.onChangeHandler} value="asdasd" />
        </div>
    )
  }
}

export default GoogleApiWrapper({
  apiKey: "AIzaSyAENgyobPFGCNGorVG-GSOKWjK2GFOMF6A"
})(Search);