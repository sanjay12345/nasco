import React from 'react';
import { GoogleApiWrapper, Map, InfoWindow, Marker } from "google-maps-react";
import {geolocated} from 'react-geolocated';
// import MyLocation from "./MyLocation";

interface IDemoProps {
  label: string;
}

// const components = {
//   MyLocation: MyLocation
// };

export class GoogleMap extends React.Component {
  	

    componentDidMount() {
    }

    state = {
      showingInfoWindow: false,
      activeMarker: {},
      selectedPlace: {},
      // latitude: '',
      // longitude: '',
    };

    onMarkerClick = (props, marker, e) =>
      this.setState({
        selectedPlace: props,
        activeMarker: marker,
        showingInfoWindow: true
    });
 
    onMapClicked = (props) => {
      if (this.state.showingInfoWindow) {
        this.setState({
          showingInfoWindow: false,
          activeMarker: null
        })
      }
    };
 
  render() {
    console.log('map-------');
    console.log(this.props);
    console.log(this.state);
  	// const { latitude, longitude } = this.state;
    return (
        <Map google={this.props.google} onClick={this.onMapClicked} initialCenter={{lat: 23.017860,lng: 72.508740}}>
          <Marker onClick={this.onMarkerClick} name={'Current location'}  />
          <InfoWindow marker={this.state.activeMarker} visible={this.state.showingInfoWindow}>
              <div>
                  <h1>{this.state.selectedPlace.name}</h1>
            </div>
          </InfoWindow>
        </Map>
    )
    // return (
    //     <MyLocation />
    // )
  }

}


class MyLocation extends React.Component<IDemoProps & GeolocatedProps> {

  constructor(props) {
      super(props);
      this.state = {
        latitude: '',
        longitude: '',
      };
  }

  render() {
      console.log('location-------');
      console.log(this.props)
      console.log(this.state)
      return !this.props.isGeolocationAvailable
        ? <div>Your browser does not support Geolocation</div>
        : !this.props.isGeolocationEnabled
          ? <div>Geolocation is not enabled</div>
          : this.props.coords
            ? <div className="map-lat-lng">
                <p>latitude : {this.props.coords.latitude} </p>
                <p>longitude : {this.props.coords.longitude}</p>
            </div>
            : <div>Getting the location data&hellip; </div>;
      // return (          
      //     <Map google={this.props.google} onClick={this.onMapClicked} initialCenter={{lat: this.state.latitude,lng: this.state.longitude}}>
      //        <Marker onClick={this.onMarkerClick} name={'Current location'}  />
      //        <InfoWindow marker={this.state.activeMarker} visible={this.state.showingInfoWindow}>
      //            <div>
      //                <h1>{this.state.selectedPlace.name}</h1>
      //          </div>
      //        </InfoWindow>
      //     </Map>
      // )
    }
}


export default GoogleApiWrapper({
  apiKey: "AIzaSyAENgyobPFGCNGorVG-GSOKWjK2GFOMF6A"
})(GoogleMap);