import React, { Component } from "react";
import { ENVIORMENT } from "./../../env";

import Slider from "react-slick";
import "slick-carousel/slick/slick.css"; 
import "slick-carousel/slick/slick-theme.css";
import "./carousel.scss";

import _ from "lodash";
import { guid } from "./../utils/Utility";

class CarouselSlide extends Component {
   project;
  constructor(props) {
    super(props);
    this.slideClick = this.slideClick.bind(this);
    this.project = (ENVIORMENT.project);
  }
  slideClick(selectedOption) {
    this.props.CarouselSelect(selectedOption);
  }
  render() {    
    const bgImg = _.get(this, "props.slide.image", "");
    let bImg;
    try {
      bImg = require(`./../../public/images/slider-menu/${this.project}/${_.get(this, "props.slide.image", "")}`);
    } catch (err) {
      bImg = require(`./../../public/images/slider-menu/${this.project}/CarouselBlankMedcare.jpg`);
    }
    
    return (
      
      <div className="card slider-bot">
      <div className="card-header">
              <h5 className={"card-title mb-0"+((_.get(this, "props.isRTLFlow", false)) ? (" font-arabic-light ") : (""))}>
                {_.get(this, "props.slide.label", "")}
              </h5>
              {
                (_.get(this, "props.slide.description", "") !== "") && 
                (
                  <p className={((_.get(this, "props.isRTLFlow", false)) ? (" font-arabic-light ") : (""))}>
                    {_.get(this, "props.slide.description", "")}
                  </p>
                )
              }              
            </div>
            
          {
            (_.get(this, "props.slide.image_type", "") === "name") &&
            (
              <div className="card-img">
                <img
                  className="card-img-top"
                  src={bImg}
                  alt=""
                /> 
              </div>
            )
          }
          {
            (_.get(this, "props.slide.image_type", "") !== "name") &&
            (
              <div className="card-img" style={{
                backgroundImage: 'url("'+(bgImg)+'")'
              }}>
              
              </div>
            )
          }
          
          <div className="card-body">
            
            
            <SlideOption
              slideOption={_.get(this, "props.slide.buttons")}
              slideClick={this.slideClick}
              isOptionSelected = {_.get(this, "props.isOptionSelected", false)}
              isRTLFlow={_.get(this, "props.isRTLFlow", false)}
              lang={_.get(this, "props.lang", "English")}
            />
          </div>
          
        </div>      
    );
  }
}

class SlideOption extends Component {
  constructor(props) {
    super(props);
    this.state = {
      selectedOption: false
    }
  }
  selectedOption(option) {
    if(
      _.get(this, "state.selectedOption", false) === false &&
      _.get(this, "props.isOptionSelected", false) === false
    ) {
      this.setState({
        selectedOption: true
      }, () => {
        this.props.slideClick(option);    
        //_.set(this, "props.slideOption", []);    
      }) 
    }
    
  }
  render() {
    const slideOption = _.get(this, "props.slideOption", []);
    return (
      <ul  key={guid()}>
        {slideOption.map((slideOption, index) => (
          <li key={guid()}>
            <a href="javascript:void(0)" className={"list-group-item cardlinks pb-2 pt-2 "+
              (_.get(this, "props.isOptionSelected", false) === true ? "disabled" : "")
              +((_.get(this, "props.isRTLFlow", false) ) ? (" font-arabic-light ") : (""))}
              onClick={() => this.selectedOption(slideOption)}>
              {slideOption.title}
            </a>
          </li>
        ))}
      </ul>
    );
  }
}

//Carousel wrapper component
class Carousel extends Component {
  constructor(props) {
    super(props);

    this.goToSlide = this.goToSlide.bind(this);
    this.goToPrevSlide = this.goToPrevSlide.bind(this);
    this.goToNextSlide = this.goToNextSlide.bind(this);

    this.state = {
      activeIndex: 0,
      isOptionSelected: false
    };
    this.CarouselSelect = this.CarouselSelect.bind(this);
  }

  goToSlide(index) {
    this.setState({
      activeIndex: index
    });
  }

  goToPrevSlide(e) {
    e.preventDefault();

    let index = this.state.activeIndex;
    let slidesLength = this.props.slides.length;

    if (index < 1) {
      index = slidesLength;
      //return false;
    }

    --index;

    this.setState({
      activeIndex: index
    });
  }

  componentDidMount() {
    if (_.get(this, "props.slides", []).length > 1 && ENVIORMENT.carousel_should_rotate === true) {
      //setInterval(e => this.goToNextSlide(true), ENVIORMENT.carousel_auto_rotate_duration);
    }    
  }

  goToNextSlide(auto=false) {
    //e.preventDefault();

    let index = this.state.activeIndex;
    let slidesLength = this.props.slides.length;

    // if (auto === false &&index === slidesLength - 1) {
    //   return false;
    // }

    if (index === slidesLength - 1) {
    //if (index === slidesLength - 1) {
      index = -1;
    }
    ++index;
    if(index > slidesLength -1) {
      index = (slidesLength -1);
    }
    this.setState({
      activeIndex: index
    });
  }

  CarouselSelect(selectedOption) {
    this.setState({
      isOptionSelected: true
    }, () => {
      this.props.slideSelect(selectedOption);  
    });
    //_.set(this, "props.slides", []);    
  }

  render() {
    const slides = _.get(this, "props.slides", []);
    let newSlides = slides;
    const settings = {
      arrows: true,
      dots: false,
      infinite: false,
      speed: 1000,
      focusOnSelect: false,
      swipeToSlide: false,
    };

    return (
      <div className={slides.length > 0 ?"nascom-slider mb-2": "nascom-slider"}>    
        <Slider {...settings}>
          {newSlides.map((slide, index) => (
            <CarouselSlide
              key= {index}
              index= {index}
              activeIndex= {_.get(this, "state.activeIndex", 0)}
              slide= {slide}
              onClick={e => this.goToSlide(index)}
              CarouselSelect= {this.CarouselSelect}
              isOptionSelected = {_.get(this, "state.isOptionSelected", false)}
              totalSlides = {slides.length}
              isRTLFlow={_.get(this, "props.isRTLFlow", false)}
              lang={_.get(this, "props.lang", "English")}
            />            
          ))}
        </Slider>        
      </div>
    );
  }
}

export default Carousel;
