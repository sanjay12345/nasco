import React from "react";
import _ from "lodash";
import "./slider.scss";

import Carousel from "./Carousel";
class Slider extends React.Component {
  constructor(props) {
    super(props);
    this.state = {};
    this.slideSelect = this.slideSelect.bind(this);
  }
  componentDidMount() {}
  componentDidUpdate() {}
  slideSelect(selectedSlideOption) {
    this.props.multiLangQuickReply(selectedSlideOption);    
  }
  render() {
    return (
      <Carousel
        slides={_.get(this, "props.sliderData.data", [])}
        slideSelect={this.slideSelect}
        isRTLFlow={_.get(this, "props.isRTLFlow", false)}
        lang={_.get(this, "props.lang", "English")}
      />
    );
  }
}

export default Slider;
