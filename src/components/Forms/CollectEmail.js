import React from "react";
import $ from "jquery";
import _ from "lodash";
// images
import iceditFill from "./../../public/images/edit_fills.svg";
import FormClose from "./../../public/images/ic_close.svg";
// css
import "./al-alain.scss";
/* Calendar Properties */
class CollectEmail extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
			email: "",
      password: "",
      isFormSent: false,
    };
  }

  componentDidMount() {
    $(".form-pay-main").animate({ bottom: "0px" });
  }

  close() {
    this.setState({
      isFormSent: true
    });
    //$("#collectEmailForm").addClass("d-none");
  }

  process() {
    if (this.isValidForm()) {
			let response = ""
			if (_.get(this, "props.mode", "") === "email") {
				response = '/email_send{"email": "' +
        _.get(this, "state.email", "") +
				'"}';
			}

			if (_.get(this, "props.mode", "") === "register_account") {
				response = '/create_account{"email": "' +_.get(this, "state.email", "") +'","password": "' +_.get(this, "state.password", "") +'"}';
			}
	    				
      this.props.userFormReply(response);
      this.close();
    }
  }

  isValidForm() {
    if (
      _.get(this, "state.email", "").match(
        /^(([^<>()[\]\\.,;:\s@"]+(\.[^<>()[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/
      ) && (_.get(this, "props.mode", "") === "email")
    ) {
      return true;
		}
		
		if (
      _.get(this, "state.email", "").match(
        /^(([^<>()[\]\\.,;:\s@"]+(\.[^<>()[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/
			) && 
			(_.get(this, "state.password", "") !== "") &&	
			(_.get(this, "props.mode", "") === "register_account")
    ) {
      return true;
		}
		
    return false;
  }

  render() {
    if(this.state.isFormSent) {
      return false;
    }
    return (
      <div id="collectEmailForm" className="form-pay-main email-form">
        <div className="form-payment text-left">
          <div className="form_close" onClick={() => this.close()}>
            <img src={FormClose} alt="" />
          </div>

          <div className="payment-cards mt-2">
            {_.get(this, "props.mode", "") === "email" && (
              <p>
                <b>
                  Please enter your email address. (Don't worry, I won't share
                  this with anyone).
                </b>
              </p>
            )}

            {_.get(this, "props.mode", "") === "register_account" && (
              <p>
                <b>
                  Great! To register with us, please enter your email address
                  and preferred password.
                </b>
              </p>
            )}

            <div className="input-group mb-3">
              <label>Email</label>
              <input
                type="email"
                className="form-control border-right-0 rounded-left"
								placeholder="Email"
								autoComplete="off"
                onChange={e => {
                  this.setState({
                    email: e.target.value
                  });
                }}
              />
              <div className="input-group-append">
                <span className="input-group-text form-input-ic">
                  <img src={iceditFill} alt="" />
                </span>
              </div>
            </div>

						 {_.get(this, "props.mode", "") === "register_account" && (
              <div className="input-group mb-3">
              <label>Password</label>
              <input
                type="password"
                className="form-control border-right-0 rounded-left"
                placeholder="Password"
                onChange={e => {
                  this.setState({
                    password: e.target.value
                  });
                }}
              />
              <div className="input-group-append">
                <span className="input-group-text form-input-ic">
                  <img src={iceditFill} alt="" />
                </span>
              </div>
            </div>
            )}

            <div className="form-group mt-4">
              <button
                className={
                  "btn btn-block btn-complete-transaction " +
                  (!this.isValidForm() ? " btn-disable " : "")
                }
                onClick={() => {
                  this.process();
                }}
              >
							 {_.get(this, "props.mode", "") === "register_account" ? "CREATE ACCOUNT" : "CONFIRM AND PROCEED" }                
              </button>
            </div>
          </div>
        </div>
      </div>
    );
  }
}

export default CollectEmail;
