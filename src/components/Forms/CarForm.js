import React from "react";
import $ from "jquery";
import _ from "lodash";
// images
import iceditFill from "./../../public/images/edit_fills.svg";
import FormClose from "./../../public/images/ic_close.svg";
// css
import "./al-alain.scss";
// rc-slider
import "rc-slider/assets/index.css";
import "rc-tooltip/assets/bootstrap.css";

import Slider from "rc-slider";

class CarForm extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      make: "",
      model: "",
      year_of_manufacture: "",
      type: "",
      price: 0,
      price_min: 0,
      price_max: 0,
      registeration_place: "",
      carTypes: [
        "Luxury",
        "Sedan",
        "MPV",
        "SUV",
        "Crossover",
        "Coupe",
        "Convertible"
      ],
      carModels: [],
      carMakes: []
    };
  }

  componentWillMount() {
    const url = "http://18.216.206.11/api/car_make";
    fetch(url)
      .then(response => response.json())
      .then(carMakes => {
        if (_.get(carMakes, "data", []).length > 0) {
          this.setState({
            carMakes: _.get(carMakes, "data", [])
          });
        }
      });
  }

  componentDidMount() {
    if (!_.isEmpty(this, "props.carDetails")) {
      this.setState(
        {
          make: _.get(this, "props.carDetails.make", ""),
          model: _.get(this, "props.carDetails.model", ""),
          year_of_manufacture: _.get(this, "props.carDetails.model_year", "2012"),
          type: _.get(this, "props.carDetails.body_style", "")
        },
        () => {
          this.getCarModels();
          this.getPriceRange();
        }
      );
    }
    $(".form-pay-main").animate({ bottom: "0px" });
  }

  getPriceRange() {
    if (
      _.get(this, "state.make", "") !== "" &&
      _.get(this, "state.model", "") !== "" &&
      _.get(this, "state.year_of_manufacture", "").length === 4
    ) {
      const url =
        "http://18.216.206.11/api/min_max?car_make=" +
        _.get(this, "state.make", "").toLowerCase() +
        "&car_model=" +
        _.get(this, "state.model", "").toLowerCase() +
        "&car_year=" +
        _.get(this, "state.year_of_manufacture", "");
      fetch(url)
        .then(response => response.json())
        .then(data => {
          this.setState({
            price_min: data.min,
            price_max: data.max
          });
        });
    }
  }

  getCarModels() {
    if (_.get(this, "state.make", "") !== "") {
      const url =
        "http://18.216.206.11/api/car_model?car_make=" +
        _.get(this, "state.make", "").toLowerCase();
      fetch(url)
        .then(response => response.json())
        .then(carModels => {
          this.setState({
            carModels: _.get(carModels, "data", []),
            price: 0,
            price_min: 0,
            price_max: 0,
          });
        });
    }
  }

  close() {
    this.props.closeDialog();
  }

  process() {
    if ( this.isFormValid() ) {
      const response =
        '/intent_confirmed_vehicle_details{"vehicle_make": "' +
        _.get(this, "state.make", "") +
        '", "vehicle_model":"' +
        _.get(this, "state.model", "") +
        '", "year_of_manufacture": "' +
        _.get(this, "state.year_of_manufacture", "") +
        '", "vehicle_type": "' +
        _.get(this, "state.type", "") +
        '", "vehicle_value":"' +
        _.get(this, "state.price", "") +
        '"}';
      this.props.sendReply(response);
      this.close();
    }
  }

  isFormValid() {
    if (
      (_.get(this, "state.make", "") !== "") &&
      (_.get(this, "state.model", "") !== "") &&
      (_.get(this, "state.year_of_manufacture", "") !== "") &&
      (_.get(this, "state.type", "") !== "") &&
      (_.get(this, "state.price", 0) > 0)
    ) {
      return true;
    }
    return false;
  }

  handleChange = sliderValue => {
    this.setState({
      price: sliderValue
    });
  };

  render() {
    const carTypes = _.get(this, "state.carTypes", []);
    const carModels = _.get(this, "state.carModels", []);
    const carMakes = _.get(this, "state.carMakes", []);
    const priceMin = _.get(this, "state.price_min", 0);
    const priceMax = _.get(this, "state.price_max", 0);
    return (
      <div className="form-pay-main car-form">
        <div className="form-payment text-left">
          <div className="form_close" onClick={() => this.close()}>
            <img src={FormClose} alt="" />
          </div>

          <div className="payment-cards mt-2">
            <p>
              <b>Please enter all car details to get an accurate quote!</b>
            </p>
            <div className="input-group mb-3">
              <label>Make</label>
              <select
                className="form-control form-dd text-capitalize"
                value={_.get(this, "state.make", "").toLowerCase()}
                onChange={e => {
                  this.setState(
                    {
                      make: e.target.value
                    },
                    () => {
                      this.getCarModels();
                    }
                  );
                }}
              >
                <option value="">Please Select</option>
                {carMakes.map((carMake, i) => (
                  <option key={i} value={carMake}>
                    {carMake}
                  </option>
                ))}
              </select>
            </div>

            <div className="input-group mb-3">
              <label>Model</label>
              <div className="col">
                <div className="row">
                  <select
                    className="form-control form-dd text-capitalize"
                    value={_.get(this, "state.model", "").toLowerCase()}
                    onChange={e => {
                      this.setState({
                        model: e.target.value
                      },() => {
                        this.getPriceRange()
                      });
                    }}
                  >
                    <option value="">Please Select</option>
                    {carModels.map((carModel, i) => (
                      <option key={i} value={carModel}>
                        {carModel}
                      </option>
                    ))}
                  </select>
                </div>
              </div>
            </div>

            <div className="input-group mb-3">
              <label>Year of manufacture (2012 - 2019)</label>
              <div className="col">
                <div className="row">
                  <select
                    className="form-control form-dd"
                    value={_.get(this, "state.year_of_manufacture", "")}
                    onChange={e => {
                      this.setState({
                        year_of_manufacture: e.target.value
                      }, () => {
                        if(_.get(this, "state.year_of_manufacture", "") !== "") {
                          this.getPriceRange()
                        }
                      });
                    }}
                  >
                    <option value="">Please Select</option>
                    <option value="2012">2012</option>
                    <option value="2013">2013</option>
                    <option value="2014">2014</option>
                    <option value="2015">2015</option>
                    <option value="2016">2016</option>
                    <option value="2017">2017</option>
                    <option value="2018">2018</option>
                    <option value="2019">2019</option>
                  </select>
                </div>
              </div>              
            </div>

            <div className="input-group mb-3">
              <label>Type</label>
              <div className="col">
                <div className="row">
                  <select
                    className="form-control form-dd"
                    value={_.get(this, "state.type", "")}
                    onChange={e => {
                      this.setState({
                        type: e.target.value
                      });
                    }}
                  >
                    <option value="">Please Select</option>
                    {carTypes.map((carType, i) => (
                      <option key={carType} value={carType}>
                        {carType}
                      </option>
                    ))}
                  </select>
                </div>
              </div>
            </div>

            <div
              className={
                "input-group m-0 pb-3" +
                (priceMin === 0 || priceMax === 0 ? " hide" : "")
              }
            >
              <label>Value (In AED)</label>
              <div className="col mt-1 pb-2">
                <Slider
                  min={priceMin}
                  max={priceMax}
                  defaultValue={priceMin}
                  onChange={this.handleChange}
                  step={100}
                />
                <div className="rc-slider-mark mt-1 pb-1">
                  <span className="rc-slider-mark-text rc-slider-mark-text-active slider-mark-left">
                    {priceMin}
                  </span>
                  <span className="rc-slider-mark-text rc-slider-mark-text-active slider-mark-right">
                    {priceMax}
                  </span>
                </div>
              </div>
            </div>

            <div className="input-group mb-3 mt-1">
              <input
                type="text"
                className="form-control border-right-0 rounded-left"
                placeholder=""
                value={_.get(this, "state.price", 10000)}
                onChange={e => {
                  _.set(this, "state.price", e.target.value);
                }}
              />
              <div className="input-group-append">
                <span className="input-group-text form-input-ic">
                  <img src={iceditFill} alt="" />
                </span>
              </div>
            </div>

            {/* <div className="input-group mb-3">
              <label>Place of Registration</label>
              <div className="col">
                <div className="row">
                  <select
                    className="form-control form-dd"
                    value={_.get(this, "state.registeration_place", "")}
                    onChange={e => {
                      this.setState({
                        registeration_place: e.target.value
                      });
                    }}
                  >
                    <option value="">Please Select</option>
                    <option value="UAE">UAE</option>
                    <option value="INDIA">INDIA</option>
                    <option value="QATAR">QATAR</option>
                    <option value="SAUDI ARABIA">SAUDI ARABIA</option>
                    <option value="MUSCAT">MUSCAT</option>
                    <option value="UNITED STATES">UNITED STATES</option>
                    <option value="UNITED KINGDOM">UNITED KINGDOM</option>
                    <option value="AUSTRALIA">AUSTRALIA</option>
                  </select>
                </div>
              </div>
            </div> */}

            <div className="form-group mt-4">
              <button
                className={"btn btn-block btn-complete-transaction " +
                (!this.isFormValid() ? "btn-disable" : "")}
                onClick={() => {
                  this.process();
                }}
              >
                CONFIRM AND PROCEED
              </button>
            </div>
          </div>
        </div>
      </div>
    );
  }
}

export default CarForm;
