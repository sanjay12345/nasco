import React from "react";
import $ from "jquery";
import _ from "lodash";

import FormClose from "./../../public/images/ic_close.svg";
import quoterequestNasco from "./../../public/images/quoterequest-nasco.svg";
import quoterequestWatania from "./../../public/images/quoterequest-watania.svg";
import quoterequestSalma from "./../../public/images/quoterequest-salma.svg";
import minusSign from "./../../public/images/minus-sign.svg";
import plusSign from "./../../public/images/plus-sign.svg";

class QuoteRequest extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      type: "",
      driver: 0,
      roadassistance: 0,
      replacement_car: 0,
      passenger: 1,
      promocode: "",
      calculate: [
        {
          name: "SALAMA",
          value: 0
        },
        {
          name: "NASCO",
          value: 0
        },
        {
          name: "WATANIA",
          value: 0
        }
      ],
      get_nasco: 0,
      get_salama: 0,
      get_watania: 0,
      isFormSent : false
    };
  }
  
  componentWillMount() {
    this.getCalculate();
  }

  componentDidMount() {
    $(".form-pay-main").animate({ bottom: "0px" });    
  }

  getCalculate() {
    let apiurl =
      "http://18.216.206.11/api/calculate?driver=" +
      _.get(this, "state.driver", "") +
      "&passenger=" +
      _.get(this, "state.passenger", "") +
      "&roadassistance=" +
      _.get(this, "state.roadassistance", "") +
      "&replacement_car=" +
      _.get(this, "state.replacement_car", "");
    fetch(apiurl)
      .then(response => response.json())
      .then(data => {
        this.setState({ calculate: data });
        let cal = _.get(data, "data", []);
        if ( cal.length > 0 ) {
          cal.map((item) => {
            if (_.get(item, "name", "") === "SALAMA") {
              return this.setState({ get_salama: item.value });
            }
            if (_.get(item, "name", "") === "NASCO") {
              return this.setState({ get_nasco: item.value });
            }
            if (_.get(item, "name", "") === "WATANIA") {
              return this.setState({ get_watania: item.value });
            }
          });
        }
      });    
  }

  process() {    
    if ( this.isFormValid() ) {
       const response = '/intent_buy_policy{"type":"'+_.get(this, "state.type", "")+
        '", "driver":"'+_.get(this, "state.driver", "")+'", "roadassistance":"'+
        _.get(this, "state.roadassistance", "")+'", "replacement_car": "'+_.get(this, "state.replacement_car", "")+
        '", "promocode": "'+_.get(this, "state.promocode", "")+
        '","passenger":"'+_.get(this, "state.passenger", "")+'"}';

       this.props.userFormReply(response);
       this.setState({
        isFormSent: true
      });
      //$("#quoteRequestForm").addClass("d-none");
    }
  }

  isFormValid() {
    if ( _.get(this, "state.type", "") !== "" ) {
      return true;
    }
    return false;
  }

  checkBoxChange = e => {
    let check = (e.target.checked) ? 1 : 0;
    this.setState({
      [e.target.name]: check
    }, () => {
      this.getCalculate();
    });
  };

  incrementCount = e => {
    this.setState({ passenger: this.state.passenger + 1 },
       () => {
        this.getCalculate();
      });      
  };

  decrementCount = e => {
    if (this.state.passenger > 1) {
      this.setState({ 
        passenger: this.state.passenger - 1 
      }, () => {
        this.getCalculate();
      });      
    }
  };

  render() {
    if (this.state.isFormSent) {
      return false;
    }
    return (
      <div id="quoteRequestForm" className="form-pay-main">
        <div className="quoterequest-form text-center">
          <div className="form_close">
            <img src={FormClose} alt="" />
          </div>
          <div className="row">
            <div className="col px-1">
              <div className="quoterequest-form-logo">
                <img src={quoterequestNasco} alt="" />
              </div>
              <div className="price-bg-blue price-sticker-bg">
                <span className="price-currency">AED</span>
                <span className="price-amount">
                  {_.get(this, "state.get_nasco", "")}/-
                </span>
              </div>
              <input
                type="radio"
                name="radiog_lite"
                id="radio1"
                className="css-checkbox"
                onChange={e => {
                  this.setState({ type: "NASCO" });
                }}
              />
              <label
                htmlFor="radio1"
                className="css-label radGroup1 radGroup1"
              />
            </div>
            <div className="col px-1">
              <div className="quoterequest-form-logo">
                <img src={quoterequestWatania} alt="" />
              </div>
              <div className="price-bg-gray price-sticker-bg">
                <span className="price-currency">AED</span>
                <span className="price-amount">
                  {_.get(this, "state.get_watania", "")}/-
                </span>
              </div>
              <input
                type="radio"
                name="radiog_lite"
                id="radio2"
                className="css-checkbox"
                onChange={e => {
                  this.setState({ type: "WATANIA" });
                }}
              />
              <label
                htmlFor="radio2"
                className="css-label radGroup1 radGroup1"
              />
            </div>
            <div className="col px-1">
              <div className="quoterequest-form-logo">
                <img src={quoterequestSalma} alt="" />
              </div>
              <div className="price-bg-cyne price-sticker-bg">
                <span className="price-currency">AED</span>
                <span className="price-amount">
                  {_.get(this, "state.get_salama", "")}/-
                </span>
              </div>
              <input
                type="radio"
                name="radiog_lite"
                id="radio3"
                className="css-checkbox"
                onChange={e => {
                  this.setState({ type: "SALAMA" });
                }}
              />
              <label
                htmlFor="radio3"
                className="css-label radGroup1 radGroup1"
              />
            </div>
          </div>

          <div className="row mt-5">
            <div className="switch-list text-left">
              <ul>
                <li>
                  <div className="switch-list-name">DRIVER COVERAGE</div>
                  <div className="pull-right">
                    <input
                      onChange={this.checkBoxChange}
                      name="driver"
                      type="checkbox"
                      id="switch1"
                    />
                    <label htmlFor="switch1" />
                  </div>
                </li>
                <li>
                  <div className="switch-list-name">ROADSIDE ASSISTANCE</div>
                  <div className="pull-right">
                    <input
                      type="checkbox"
                      onChange={this.checkBoxChange}
                      name="roadassistance"
                      id="switch2"
                    />
                    <label htmlFor="switch2" />
                  </div>
                </li>
                <li>
                  <div className="switch-list-name">REPLACEMENT CAR</div>
                  <div className="pull-right">
                    <input
                      onChange={this.checkBoxChange}
                      type="checkbox"
                      name="replacement_car"
                      id="switch3"
                    />
                    <label htmlFor="switch3" />
                  </div>
                </li>
                <li>
                  <div className="switch-passengers-name">PASSENGERS</div>
                  <div className="switch-passengers pull-right">
                    <span onClick={this.decrementCount} className="minus-sign">
                      {" "}
                      <img src={minusSign} alt="" />{" "}
                    </span>
                    <input
                      onChange={e => {
                        _.set(this, "state.passenger", e.target.value);
                      }}
                      name="passenger"
                      value={_.get(this, "state.passenger", "")}
                      readOnly
                      type="text"
                      className="passengers-number"
                    />
                    <span onClick={this.incrementCount} className="plus-sign">
                      {" "}
                      <img src={plusSign} alt="" />{" "}
                    </span>
                  </div>
                </li>
                <li>
                  <div className="switch-passengers-name">PROMO CODE</div>
                  <div className="switch-passengers pull-right">
                    <input name="promocode" type="text" className="passengers-number txt-promocode" value={_.get(this, "state.promocode", "")}
                    onChange={e => {
                      this.setState({
                        promocode: e.target.value
                      })
                    }}
                     />
                  </div>
                </li>
              </ul>
            </div>
          </div>
          <div className="form-group mt-5">
            <button
              className={ "btn btn-block btn-complete-transaction "+ ( !this.isFormValid() ? "btn-disable" : "")}
              onClick={() => {
                this.process();
              }}
            >
              CONFIRM AND PROCEED
            </button>
          </div>
        </div>
      </div>
    );
  }
}

export default QuoteRequest;
