import React from "react";
import $ from "jquery";
import _ from "lodash";
import { dateToDBFormat } from "./../utils/Utility";
// images
import iceditFill from "./../../public/images/edit_fills.svg";
import FormClose from "./../../public/images/ic_close.svg";
// css
import "./al-alain.scss";
import "./date-picker.scss";
/* Calendar Properties */
import Calendar from "rc-calendar";
import DatePicker from "rc-calendar/lib/Picker";
import moment from "moment";
const format = "DD-MM-YYYY";
const now = moment().subtract(18, 'year');
now.locale("en-gb").utcOffset(0);
function getFormat(time) {
  return time ? format : "DD/MM/YYYY";
}

// const defaultCalendarValue = now;
// const defaultCalendarValue = now.clone();
// defaultCalendarValue.add(-1, 'month');

const multiFormats = ["DD/MM/YYYY"];

class DrivingLicenceForm extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      name: "",
      nationality: "",
      dob: new Date().setFullYear(new Date().getFullYear() - 18),
      licence_number: "",
      validity: "",
      value: ""
    };
  }

  onChange = value => {
    this.setState({
      value
    });
  };

  onChangeValidity = validity => {
    this.setState({
      validity
    });
  };

  onStandaloneSelect(value) {
    console.log(value && value.format(format));
  }

  onStandaloneChange(value) {
    console.log(value && value.format(format));
  }

  disabledDate(current) {
    if (!current) {
      // allow empty select
      return false;
    }
    const date = moment().subtract(18, "years");
    date.hour(0);
    date.minute(0);
    date.second(0);

    if (_.get(this, "props.chat.range", "") === "future") {
      return current.valueOf() < date.valueOf(); // can select future days
    } else {
      return current.valueOf() > date.valueOf(); // can select past days
    }
  }

  disabledfutureDate(current) {
    if (!current) {
      // allow empty select
      return false;
    }
    const date = moment();
    date.hour(0);
    date.minute(0);
    date.second(0);
    return current.valueOf() < date.valueOf(); // can select future days
  }

  onShowDateInputChange = e => {
    this.setState({
      showDateInput: e.target.checked
    });
  };

  componentDidMount() {
    $(".form-pay-main").animate({ bottom: "0px" });
  }

  close() {
    this.props.closeDialog("");
  }

  process() {
    if (this.isFormValid()) {
      const response =
        '/intent_confirmed_license_details{"license_name": "' +
        _.get(this, "state.name", "") +
        '", "license_dob":"' +
        dateToDBFormat(this.state.value.format("YYYY-MM-DD")) +
        '", "license_number": "' +
        _.get(this, "state.licence_number", "") +
        '", "license_validity": "' +
        dateToDBFormat(this.state.validity.format("YYYY-MM-DD")) +
        '"}';
      this.props.sendReply(response);
      this.close();
    }
  }

  isFormValid() {
    if (
      _.get(this, "state.name", "") !== "" &&
      _.get(this, "state.value", "") !== "" &&
      _.get(this, "state.licence_number", "") !== "" &&
      _.get(this, "state.validity", "") !== ""
    ) {
      return true;
    }
    return false;
  }

  render() {
    const state = this.state;
    const calendar = (
      <Calendar
        style={{ zIndex: 1000 }}
        disabledDate={this.disabledDate.bind(this)}
        format={multiFormats}
        defaultValue={now}
        showDateInput={state.showDateInput}
      />
    );
    const futureCalendar = (
      <Calendar
        style={{ zIndex: 1000 }}
        format={multiFormats}
        disabledDate={this.disabledfutureDate.bind(this)}
        defaultValue={this.props.defaultCalendarValue}
        showDateInput={state.showDateInput}
      />
    );
    return (
      <div className="form-pay-main licence-form">
        <div className="form-payment text-left">
          <div className="form_close" onClick={() => this.close()}>
            <img src={FormClose} alt="" />
          </div>

          <div className="payment-cards mt-2">
            <div className="input-group mb-3">
              <label>Name</label>
              <input
                type="email"
                className="form-control border-right-0 rounded-left"
                placeholder="Name"
                value={_.get(this, "state.name", "")}
                onChange={e => {
                  if (e.target.value.match(/^[a-zA-Z- ]*$/)) {
                    this.setState({
                      name: e.target.value
                    });
                  }
                }}
              />
              <div className="input-group-append">
                <span className="input-group-text form-input-ic">
                  <img src={iceditFill} alt="" />
                </span>
              </div>
            </div>

            {/* <div className="input-group mb-3">
              <label>Nationality</label>
              <div className="col">
                <div className="row">
                  <select
                    className="form-control form-dd"
                    onChange={e => {
                      _.set(this, "state.nationality", e.target.value);
                    }}
                  >
                    <option value="">Please Select</option>
                    <option value="UAE">UAE</option>
                    <option value="INDIA">INDIA</option>
                    <option value="QATAR">QATAR</option>
                    <option value="SAUDI ARABIA">SAUDI ARABIA</option>
                    <option value="MUSCAT">MUSCAT</option>
                    <option value="UNITED STATES">UNITED STATES</option>
                    <option value="UNITED KINGDOM">UNITED KINGDOM</option>
                    <option value="AUSTRALIA">AUSTRALIA</option>
                  </select>
                </div>
              </div>
            </div> */}

            <div className="input-group mb-3">
              <label>Date of birth</label>
              <DatePicker
                animation="slide-up"
                calendar={calendar}
                onChange={this.onChange}
                readOnly
              >
                {({ value }) => {
                  return (
                    <div className="col">
                      <span tabIndex="0" className="row">
                        <input
                          placeholder="DD/MM/YYYY"
                          className="ant-calendar-picker-input form-control border-right-1 rounded-left ant-input datedate"
                          value={
                            (value &&
                              value.format(getFormat(state.showTime))) ||
                            ""
                          }
                          readOnly
                        />
                      </span>
                    </div>
                  );
                }}
              </DatePicker>
            </div>

            <div className="input-group mb-3">
              <label>Licence Number</label>
              <input
                type="text"
                className="form-control border-right-0 rounded-left"
                placeholder="Licence Number"
                value={_.get(this, "state.licence_number", "")}
                onChange={e => {
                  if (e.target.value.match(/^[1-9]*$/)) {
                    this.setState({
                      licence_number: e.target.value
                    });
                  }
                }}
              />
              <div className="input-group-append">
                <span className="input-group-text form-input-ic">
                  <img src={iceditFill} alt="" />
                </span>
              </div>
            </div>

            <div className="input-group mb-3">
              <label>Validity</label>
              <DatePicker
                animation="slide-up"
                calendar={futureCalendar}
                onChange={this.onChangeValidity}
                readOnly
              >
                {({ value }) => {
                  return (
                    <div className="col">
                      <span tabIndex="0" className="row">
                        <input
                          placeholder="DD/MM/YYYY"
                          className="ant-calendar-picker-input form-control border-right-1 rounded-left ant-input datedate"
                          value={
                            (value &&
                              value.format(getFormat(state.showTime))) ||
                            ""
                          }
                          readOnly
                        />
                      </span>
                    </div>
                  );
                }}
              </DatePicker>
            </div>

            <div className="form-group mt-4">
              <button
                className={
                  "btn btn-block btn-complete-transaction " +
                  (!this.isFormValid() ? "btn-disable" : "")
                }
                onClick={() => {
                  this.process();
                }}
              >
                CONFIRM AND PROCEED
              </button>
            </div>
          </div>
        </div>
      </div>
    );
  }
}

export default DrivingLicenceForm;
