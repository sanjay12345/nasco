import React from "react";
import $ from "jquery";
import _ from "lodash";

import iclock from "./../../public/images/lock.svg";
import cardsm from "./../../public/images/cards-m.png";
import cardsv from "./../../public/images/cards-v.png";
import slashIcon from "./../../public/images/date-slash.png";
import FormClose from "./../../public/images/ic_close.svg";

class PaymentStart extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      card: "",
      card_number: "",
      name: "",
      expire_month: "",
      expire_year: "",
      cvv: "",
      isFormSent : false
    };
  }

  componentDidMount() {
    $(".form-pay-main").animate({ bottom: "0px" });
  }

  process() {    
    if ( this.isFormValid() ) {    
      const response = '/intent_paid{"card":"'+_.get(this, "state.card", "")+'","card_number":"'+_.get(this, "state.card_number", "")+'","card_name":"'+_.get(this, "state.name", "")+'", "expire_month":"'+_.get(this, "state.expire_month", "")+'", "expire_year": "'+_.get(this, "state.expire_year", "")+'", "cvv": "'+_.get(this, "state.cvv", "")+'"}'
      // this.props.userFormReply(JSON.stringify(response));
      this.props.userFormReply(response)
      this.setState({
        isFormSent: true
      });
    } 
  }

  isFormValid() {
    if (
      ( _.get(this, "state.card", "") !== "" ) &&
      ( _.get(this, "state.card_number", "").length === 16 ) &&
      ( _.get(this, "state.name", "") !== "" ) &&
      ( _.get(this, "state.expire_month", "") !== "" ) &&
      ( _.get(this, "state.cvv", "").length === 3 )
    ) {    
      return true;
    }
    return false;
  }

  render() {
    if(this.state.isFormSent) {
      return false;
    }
    return (
      <div id="frmPayment" className="form-pay-main">
        <div className="form-payment text-left">
          <div className="form_close">
            <img src={FormClose} alt="" />
          </div>
          <div className="secure-text">
            <span className="float-left">
              <img src={iclock} alt="" className="iclock" />
            </span>
            <h1>Secure Payment Info</h1>
            <h6>This is a secure 128-bit SSL Encrypted payment.</h6>
          </div>
          <div className="payment-cards">
            <input
              type="radio"
              name="radiog_lite"
              id="radio1"
              className="css-checkbox"
              onChange = {e => { this.setState({ card: "MASTERCARD" }); } }
            />
            <label htmlFor="radio1" className="css-label radGroup1 radGroup1" />
            <span className="cards-type">
              <span className="mr-2">
                <img src={cardsm} alt="" />
              </span>
              <input
                type="radio"
                name="radiog_lite"
                id="radio2"
                className="css-checkbox"
                onChange = {e => { this.setState({ card: "VISA" }); } }
              />

              <label
                htmlFor="radio2"
                className="css-label radGroup1 radGroup1"
              />
              <span>
                <img src={cardsv} alt="" />
              </span>
            </span>

            <div className="cards-form">
              <div className="form-group">
                <label>Name as it appears on the card</label>
                <input
                  type="text"
                  className="form-control"
                  placeholder="Enter name"
                  value= {_.get(this, "state.name", "")}
                  onChange={e => {
                    if ((e.target.value).match(/^[a-zA-Z- ]*$/)) {
                      this.setState({
                        name: e.target.value
                      })  
                    }
                  }}
                />
              </div>
              <div className="form-group">
                <label>Card Number (16 digits on the front of the card)</label>
                <input
                  type="text"
                  className="form-control"
                  placeholder="XOXO - XOXO - XOXO - XOXO"
                  value = {_.get(this, "state.card_number", "")}
                  maxLength="16"
                  onChange = { (e) => {
                    const re = /^[0-9\b]+$/;
                    if (re.test(e.target.value) || e.target.value === "") {
                      if ((_.get(this, "state.card_number", "").length > 16) && (e.target.value !== "") ) {
                        return false;
                      }
                      this.setState({
                        card_number: e.target.value
                      })

                  } }}
                />
              </div>
              <div className="row">
                <div className="col-8">
                  <label>Expiration date</label>
                  <div className="formdd_m">
                    <select className="form-control" onChange={e => {
                      _.set(this, "state.expire_month", e.target.value);
                    }}>
                      <option value="">Month</option>
                      <option value="1">1</option>
                      <option value="2">2</option>
                      <option value="3">3</option>
                      <option value="4">4</option>
                      <option value="5">5</option>
                      <option value="6">6</option>
                      <option value="7">7</option>
                      <option value="8">8</option>
                      <option value="9">9</option>
                      <option value="10">10</option>
                      <option value="11">11</option>
                      <option value="12">12</option>
                    </select>
                  </div>
                  <span>
                    <img src={slashIcon} alt="" />{" "}
                  </span>
                  <div className="formdd_y">
                    <select className="form-control" onChange={e => {
                      _.set(this, "state.expire_year", e.target.value);
                    }}>
                      <option value="">Year</option>
                      <option value="2019">2019</option>
                      <option value="2020">2020</option>
                      <option value="2021">2021</option>
                      <option value="2022">2022</option>
                      <option value="2023">2023</option>
                      <option value="2024">2024</option>
                      <option value="2025">2025</option>
                      <option value="2026">2026</option>
                      <option value="2027">2027</option>
                      <option value="2028">2028</option>
                      <option value="2029">2029</option>
                    </select>
                  </div>
                </div>
                <div className="col-4 pl-0">
                  <label className="text-left">CVV2/CVC2</label>
                  <input
                    type="password"
                    className="form-control float-right"
                    placeholder=""
                    maxLength="3"
                    value = {_.get(this, "state.cvv", "")}
                    onChange={e => {
                      const re = /^[0-9\b]+$/;
                      if (re.test(e.target.value) || e.target.value === "") {
                        this.setState({
                          cvv: e.target.value
                        })
                      }                      
                    }}
                  />
                  <span>
                    <label>
                      The last 3 digits displayed on the back of your card
                    </label>
                  </span>
                </div>
              </div>

              <div className="form-group mt-4">
                <button className={"btn btn-block btn-complete-transaction " + (!this.isFormValid() ? "btn-disable" : "" )}
                  onClick = { () => this.process()}
                >
                  Complete Transaction
                </button>
              </div>
            </div>
          </div>
        </div>
      </div>
    );
  }
}

export default PaymentStart;
