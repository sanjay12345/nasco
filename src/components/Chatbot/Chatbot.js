import React from "react";
import _ from "lodash";
import InitialScreen from "./../InitalScreen/InitialScreen";
import ChatScreen from "./../ChatScreen/ChatScreen";
import { ENVIORMENT } from "./../../env";

// import Draggable from 'react-draggable';
// Style sheet
import "./chatbot.scss";

class Chatbot extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      is_bot_visible: false,
      is_conversation_start: false,
      selectedAvatar: "Granny",
      avatarDispName: "Granny",
      isRTLFlow: false,
      language: "English",
      moveMe: {},
      face: {},
      message: {}
    };

    this.loadConversationScreen = this.loadConversationScreen.bind(this);
    this.endConversation = this.endConversation.bind(this);
    this.toggleRTL = this.toggleRTL.bind(this);
    this.toggleBotButton = this.toggleBotButton.bind(this);
  }

  /*
    Toggle display of chat screen
  */
  toggleBotButton() {
    this.setState({ is_bot_visible: !this.state.is_bot_visible });
  }

  endConversation() {
    this.setState({
      is_bot_visible: false,
      is_conversation_start: false,
      isRTLFlow: false,
      language: "English"
    });
  }

  toggleRTL(lang) {
    if (_.indexOf(ENVIORMENT.rtl_languages, lang) >= 0) {
      this.setState({
        isRTLFlow: true,
        language: lang
      });
    }
  }

  /*
    load conversation screen on click of start conversation button
  */
  loadConversationScreen(avatar = "Granny", avatarDispName = "Granny") {
    this.setState({
      is_conversation_start: !this.state.is_conversation_start,
      avatarDispName: avatarDispName,
      selectedAvatar: avatar
    });
  }

  componentDidMount() {
    this.toggleBotButton();
    this.setState(
      {
        moveMe: document.getElementById("movable"),
        face: document.getElementById("face"),
        message: document.getElementById("message")
      },
      () => {}
    );
  }
  render() {
    return (
      <div
        className={
          _.get(this, "state.isRTLFlow", false) === true ? "lang-rtl" : ""
        }
      >
        <div className={this.state.is_bot_visible ? "" : "hide"}>
          {/* <Draggable axis="x" bounds="body"> */}
          <div className={"main-chat-box"}>
            {!this.state.is_conversation_start && (
              <InitialScreen
                loadConversationScreen={this.loadConversationScreen}
                toggleRTL={this.toggleRTL}
                endConversation={this.endConversation}
              />
            )}
            {this.state.is_conversation_start && (
              <ChatScreen
                endConversation={this.endConversation}
                avatar={_.get(this, "state.selectedAvatar", "")}
                isConversationOpen={_.get(this, "state.is_bot_visible", false)}
                toggleRTL={this.toggleRTL}
                isRTLFlow={_.get(this, "state.isRTLFlow", false)}
                avatarDispName={_.get(this, "state.avatarDispName", "")}
                lang={_.get(this, "state.language", "English")}
                toggleBotButton={this.toggleBotButton}
              />
            )}
          </div>
          {/* </Draggable>         */}
        </div>
        <div
          className={
            "bottom-toggle " + (this.state.is_bot_visible ? "hide" : "")
          }
        >
          <div
            className={
              "icon_ic chat_ic " + (this.state.is_bot_visible ? "hide" : "")
            }
            onClick={() => this.toggleBotButton()}
          />
          <div
            className={
              "icon_ic close_ic " + (this.state.is_bot_visible ? "" : "hide")
            }
            onClick={() => this.toggleBotButton()}
          />
        </div>
      </div>
    );
  }
}
export default Chatbot;
